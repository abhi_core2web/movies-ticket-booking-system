package com.ticket_booking_application.DashBoard;

import com.ticket_booking_application.Controller.*;
import com.ticket_booking_application.services.Bit_APISearchMovie;

import javafx.animation.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Landing extends Application {

    Stage stage;

    private Scene homeScene, loginScene, paymentScene, locationScene, movieInfoScene, theatreLocationScene,userInfoScene;

    Login login;
    Location location;
    Home home;
    MovieInfo movieInfo;
    TheatreLocation theatreLocation;
    Payment payment;
    UserInfo userInfo;

    private Label bit_Label3;

    // Array of text messages to display sequentially
    private String[] messages = {
        "Book your tickets now and enjoy the latest blockbusters",
        "Experience the thrill of blockbuster movies",
        "Discover new series and binge-worthy content",
        "Immerse yourself in the world of cinema and popcorn",
        "Enjoy a cinematic experience like never before",
        "Discover hidden gems and classic favorites",
    };

    Bit_APISearchMovie bit_APISearchMovie = new Bit_APISearchMovie();
    // Index to track current message
    private int messageIndex = 0;


    @Override
    public void start(Stage stage) {
        this.stage = stage;

        login = new Login(this);
        location = new Location(this);
        movieInfo = new MovieInfo(this); // Ensure this is not null
        home = new Home(this);
        theatreLocation = new TheatreLocation(this);
        payment = new Payment(this);
        userInfo = new UserInfo(this);
    
        homeScene = new Scene(home.getHomePageScene(), 1920, 1000);
        loginScene = new Scene(login.getLoginScene(), 1920, 1000);
        locationScene = new Scene(location.getlocationScene(), 1920, 1000);
        movieInfoScene = new Scene(movieInfo.getMovieScene(), 1920, 1000);
        theatreLocationScene = new Scene(theatreLocation.getTheatreLocationScene(), 1920, 1000);
        paymentScene = new Scene(payment.getPaymentScene(), 1920, 1000);
        userInfoScene = new Scene(userInfo.getUserInfoScene(), 1920, 1000);

        stage.setTitle("Ticket Booking Application");
        stage.getIcons().add(new Image("Images/cinema.png"));

        Media media = new Media(getClass().getResource("/Images/bgc2.mp4").toExternalForm());
        MediaPlayer mediaPlayer = new MediaPlayer(media);
        MediaView mediaView = new MediaView(mediaPlayer);
        mediaView.setPreserveRatio(true);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);

        Label bit_Label1 = new Label("Welcome!");
        bit_Label1.setFont(new Font("Arial", 80));
        bit_Label1.setStyle("-fx-text-fill: #3cdae6; -fx-font-weight: bold;");
        StackPane.setAlignment(bit_Label1, Pos.CENTER_LEFT);
        StackPane.setMargin(bit_Label1, new Insets(0, 0, 250, 50));

        Label bit_Label2 = new Label("Experience the Magic of Movies");
        bit_Label2.setFont(new Font("Arial", 40));
        bit_Label2.setStyle("-fx-text-fill: #3cdae6; -fx-font-weight: bold; -fx-font-style: italic;");
        StackPane.setAlignment(bit_Label2, Pos.CENTER_LEFT);
        StackPane.setMargin(bit_Label2, new Insets(0, 0, 80, 50));

        bit_Label3 = new Label(messages[0]);
        bit_Label3.setFont(new Font("Arial", 30));
        bit_Label3.setStyle("-fx-text-fill: white; -fx-font-weight: bold;");
        StackPane.setAlignment(bit_Label3, Pos.CENTER_LEFT);
        StackPane.setMargin(bit_Label3, new Insets(150, 0, 0, 50));

        // Create a Timeline to change text messages sequentially
        EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                changeText();
            }
        };
        KeyFrame keyFrame = new KeyFrame(Duration.seconds(2), eventHandler);
        Timeline timeline = new Timeline(keyFrame);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        Button bit_getStartedBtn = new Button("Get Started" + "➡️");
        bit_getStartedBtn.setMaxHeight(60);
        bit_getStartedBtn.setMaxWidth(300);
        bit_getStartedBtn.setCursor(Cursor.HAND);
        bit_getStartedBtn.setStyle("-fx-background-color: RED; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30;");
        bit_getStartedBtn.setOnAction(e -> {
            redirectToLogin();
        });

        StackPane.setAlignment(bit_getStartedBtn, Pos.CENTER_LEFT);
        StackPane.setMargin(bit_getStartedBtn, new Insets(400, 0, 0, 50));

        VBox bit_stack = new VBox(mediaView);

        StackPane root = new StackPane(bit_stack, bit_Label1, bit_Label2, bit_Label3, bit_getStartedBtn);
        Scene scene = new Scene(root, 1920, 1000);
        stage.setScene(scene);

        stage.show();

        mediaPlayer.play();
    }

    // Method to change text in bit_Label3 sequentially
    private void changeText() {
        messageIndex = (messageIndex + 1) % messages.length; // Increment index in a loop
        String message = messages[messageIndex];

        // Fade out current text
        FadeTransition fadeOut = new FadeTransition(Duration.seconds(1), bit_Label3);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);

        // Set new text and fade in
        fadeOut.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                bit_Label3.setText(message);
                FadeTransition fadeIn = new FadeTransition(Duration.seconds(1), bit_Label3);
                fadeIn.setFromValue(0.0);
                fadeIn.setToValue(1.0);
                fadeIn.play();
            }
        });

        fadeOut.play();
    }

    public void redirectToHome() {
        if (AppState.getInstance().getLoggedInUsername() != null) {
            home.bit_isLogin(true);
        } else {
            home.bit_isLogin(false);
        }
        home.bit_setMovieImages("action", "hindi", 30);
        stage.setScene(homeScene);
    }
    
    public void redirectToLogin() {
            stage.setScene(loginScene);
    }
    
    public void redirectToLocation() {
        stage.setScene(locationScene);
    }
    
    public void redirectToMovieInfo(String movieName) {
    
        if (movieInfo == null) {
            System.out.println("Error: movieInfo is null");
            return;
        }
        if (AppState.getInstance().getLoggedInUsername() != null) {
            movieInfo.bit_isLogin(true);
        } else {
            movieInfo.bit_isLogin(false);
        }
        System.out.println("Redirecting to movie info for: " + movieName);
        movieInfo.bit_initTheInfoPage(movieName);
        stage.setScene(movieInfoScene);
    }
    
    
    public void redirectToTheater() {
        if (AppState.getInstance().getLoggedInUsername() != null) {
            theatreLocation.bit_isLogin(true);
        } else {
            theatreLocation.bit_isLogin(false);
        }
        theatreLocation.bit_setTheatreLocation();
        stage.setScene(theatreLocationScene);
    }
    
    public void redirectToPayment() {
        if (AppState.getInstance().getLoggedInUsername() != null) {
            payment.bit_isLogin(true);
            payment.update();
        } else {
            payment.bit_isLogin(false);
        }
        stage.setScene(paymentScene);
    }

    public void redirectToUserInfo() {
        if (AppState.getInstance().getLoggedInUsername() != null) {
            userInfo.bit_isLogin(true);
        } else {
            userInfo.bit_isLogin(false);
        }
        stage.setScene(userInfoScene);
    }
       
    public void Bit_currentPage(String previousPage) {
        if (previousPage.equals("Login")) {
            stage.setScene(loginScene);
        } else if (previousPage.equals("Home")) {
            stage.setScene(locationScene);
        } else if (previousPage.equals("MovieInfo")) {
            redirectToMovieInfo("movieName");
        } else if (previousPage.equals("TheatreLocation")) {
            redirectToTheater();
        } else if (previousPage.equals("Payment")) {
            redirectToPayment();
        } else if (previousPage.equals("Location")) {
            redirectToLocation();
        }
    }
}