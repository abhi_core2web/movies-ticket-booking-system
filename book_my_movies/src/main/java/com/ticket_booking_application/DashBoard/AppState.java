package com.ticket_booking_application.DashBoard;



public class AppState {
    private static AppState instance = new AppState();  //Creating an instance of the class
    private boolean isLoggedIn;
    private String loggedInUsername;
    private String loggedInUserFullName;
    private String location;
    private String date;    //set in Login file
    private String movieName;   //set in MovieInfo file
    private String theatreName; //set in TheatreLocation file
    private String[] seatNumber;    //setq in TheatreLocation file
    private String subTotal;    //set in TheatreLocation file
    private String movieLanguage;  //set in MovieInfo file
    private String moviePoster; //set in MovieInfo file
    private String movieRunTime;    //set in MovieInfo file
    private String movieTime;   //set in TheatreLocation file

    private AppState() {
    }

    public static AppState getInstance() {
        return instance;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public String getLoggedInUsername() {
        return loggedInUsername;
    }

    public void setLoggedInUsername(String loggedInUsername) {
        this.loggedInUsername = loggedInUsername;
    }

    public String getLoggedInUserFullName() {
        return loggedInUserFullName;
    }

    public void setLoggedInUserFullName(String loggedInUserFullName) {
        this.loggedInUserFullName = loggedInUserFullName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getTheatreName() {
        return theatreName;
    }

    public void setTheatreName(String theatreName) {
        this.theatreName = theatreName;
    }


    public String[] getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String[] seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getMovieLanguage() {
        return movieLanguage;
    }

    public void setMovieLanguage(String movieLanguage) {
        this.movieLanguage = movieLanguage;
    }

    public String getMoviePoster() {
        return moviePoster;
    }

    public void setMoviePoster(String moviePoster) {
        this.moviePoster = moviePoster;
    }

    public String getMovieRunTime() {
        return movieRunTime;
    }

    public void setMovieRunTime(String movieRunTime) {
        this.movieRunTime = movieRunTime;
    }
    
    public String getMovieTime() {
        return movieTime;
    }

    public void setMovieTime(String movieTime) {
        this.movieTime = movieTime;
    }
}
