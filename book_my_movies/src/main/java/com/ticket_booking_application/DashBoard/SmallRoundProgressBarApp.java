package com.ticket_booking_application.DashBoard;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class SmallRoundProgressBarApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setVisible(false); // Initially hidden
        progressIndicator.setPrefSize(40, 40); // Set smaller size

        Button button = new Button("Show Progress");
        button.setOnAction(event -> {
            progressIndicator.setVisible(true);
            // Create a timeline to hide the progress indicator after 3 seconds
            Timeline timeline = new Timeline(new KeyFrame(
                Duration.seconds(3),
                ae -> progressIndicator.setVisible(false)
            ));
            timeline.play();
        });

        StackPane root = new StackPane();
        root.getChildren().addAll(progressIndicator, button);
        StackPane.setAlignment(button, Pos.BOTTOM_CENTER);
        StackPane.setAlignment(progressIndicator, Pos.CENTER);

        Scene scene = new Scene(root, 1800, 900);

        primaryStage.setTitle("Small Round Progress Bar Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
