package com.ticket_booking_application.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

public class Bit_APIFetchNearLocation {
    private static final String LOCATIONIQ_API_KEY = "pk.1230f9cfb29cb6c01351aed78a20654b";
    private static final String LOCATIONIQ_BASE_URL = "https://us1.locationiq.com/v1/";

    public static String[] getCoordinates(String searchString) {
        String[] coordinates = new String[2];

        HttpURLConnection conn = null;
        BufferedReader in = null;

        try {
            String urlStr = LOCATIONIQ_BASE_URL + "search.php?key=" + LOCATIONIQ_API_KEY + "&q=" + searchString + "&format=json&limit=1";
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder content = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                JSONArray jsonArray = new JSONArray(content.toString());
                if (jsonArray.length() > 0) {
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    coordinates[0] = jsonObject.getString("lat");
                    coordinates[1] = jsonObject.getString("lon");
                }
            } else {
                System.err.println("HTTP request failed with response code: " + conn.getResponseCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return coordinates;
    }


    public static String[] fetchNearbyLocations(String lat, String lon) {
        String[] nearbyLocations = new String[0];

        HttpURLConnection conn = null;
        BufferedReader in = null;

        try {
            String urlStr = LOCATIONIQ_BASE_URL + "nearby.php?key=" + LOCATIONIQ_API_KEY + "&lat=" + lat + "&lon=" + lon + "&tag=place&radius=20000&limit=50";
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder content = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                JSONArray jsonArray = new JSONArray(content.toString());
                int count = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("name")) {
                        String name = jsonObject.getString("name");
                        if (!containsNumber(name)) { // Check if name contains any number
                            count++;
                        }
                    }
                }

                nearbyLocations = new String[count];
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("name")) {
                        String name = jsonObject.getString("name");
                        if (!containsNumber(name)) { // Check if name contains any number
                            nearbyLocations[index++] = name;
                        }
                    }
                }
            } else {
                System.err.println("HTTP request failed with response code: " + conn.getResponseCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return nearbyLocations;
    }

    // Helper method to check if a string contains any digit
    private static boolean containsNumber(String str) {
        return str.matches(".*\\d.*");
    }
}