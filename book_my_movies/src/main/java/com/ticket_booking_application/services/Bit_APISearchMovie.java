package com.ticket_booking_application.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;


public class Bit_APISearchMovie {

    private static final String API_KEY = "apikey=18c3789c";
    private static final String BASE_URL = "http://www.omdbapi.com/?";

    public static JSONObject bit_fetchMovies(String movieName) {

        try {
            String urlStr = BASE_URL + API_KEY + "&" + "t=" + movieName;
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            in.close();
            conn.disconnect();

            JSONObject movieInfo = new JSONObject(content.toString());
            return movieInfo;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
