package com.ticket_booking_application.services;

import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Cursor;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class Bit_AutoSlidingImageWindow {


    public static ScrollPane bit_createAutoSlidingImageWindow() {
        // Create an HBox to hold the images
        HBox imageStore = new HBox(10);

        List<Movies> bit_moviesList = Bit_APIFetchMovies.getMoviesList(); // Replace with your actual API call

        imageStore.getChildren().clear(); // Clear any existing movie images

        for (Movies movie : bit_moviesList) {

            Image bit_SlidngImages = new Image("https://image.tmdb.org/t/p/w500"+movie.getupcomingMovieUrl());
            ImageView bit_slidingImageView = new ImageView(bit_SlidngImages);
            bit_slidingImageView.setFitHeight(300);
            bit_slidingImageView.setFitWidth(250);
            

            // Label bit_slidingMovieName = new Label(movie.getupcomingMovieName());
            // bit_slidingMovieName.setStyle("-fx-text-fill: red; -fx-font-size: 20; -fx-font-weight: bold;");
            // StackPane.setAlignment(bit_slidingMovieName, Pos.TOP_CENTER);
            StackPane bit_slidingImagesStack = new StackPane(bit_slidingImageView);
            bit_slidingImagesStack.setCursor(Cursor.HAND);
           
            imageStore.getChildren().add(bit_slidingImagesStack);
        }

        // Create a ScrollPane and set the HBox as its content
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(imageStore);
        scrollPane.setFitToHeight(true); // Fit the height of the scroll pane to its content
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide the horizontal scrollbar
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide the vertical scrollbar

        KeyFrame keyFrame = new KeyFrame(Duration.millis(1000), event -> {
            // Check if the scrollPane has reached or exceeded its maximum Hvalue
            if (scrollPane.getHvalue() >= 1.0) {
                // Reset to start
                scrollPane.setHvalue(0);
            } else {
                // Scroll to the next image
                scrollPane.setHvalue(scrollPane.getHvalue() + 0.1);
            }
        });

        // Create a Timeline for automatic sliding
        Timeline timeline = new Timeline(keyFrame);
        timeline.setCycleCount(Timeline.INDEFINITE); // Repeat indefinitely
        timeline.play(); // Start the animation

        return scrollPane;
    }
}
