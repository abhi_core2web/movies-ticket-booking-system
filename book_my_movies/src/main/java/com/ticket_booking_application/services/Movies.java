package com.ticket_booking_application.services;


public class Movies {

    String movieName;   //It's a variable to store the name of the movie for searching
    double movieRating; //It's a variable to store the rating of the movie for Home Page Main Images
    String movieposterUrl;  //It's a variable to store the poster of the movie for Home Page Main Images

    String upcomingMovieUrl;    //It's a variable to store the poster of the upcoming movie for Home Page Slider
    String upcomingName;    //It's a variable to store the name of the upcoming movie for Home Page Slider

    String location;    //It's a variable to store the location of the user
    int data;   //It's a variable to store the date of the movie

    String movieTitle;   //It's a variable to store the title of the movie for Theater Location
    String movieRunTime; //It's a variable to store the run time of the movie for Theater Location
    String movieGenre;   //It's a variable to store the genre of the movie for Theater Location
    String movieImgUrl;  //It's a variable to store the image of the movie for Theater Location


    //This is a default constructor
    public Movies() {
    }

    //This Constructor is used to get the location and date of the movie
    // public Movies(String location, int data) {  
    //     this.location = location;
    //     this.data = data;

    // }

    //This Constructor is used to get the upcoming movie name and it's poster for Displaying on Home slider
    public  Movies(String upcomingMovieUrl, String upcomingName){
        this.movieposterUrl = upcomingMovieUrl;
        this.movieName = upcomingName;
    }

    //This Constructor is used to get the movie name, rating and it's poster for Displaying on Home main Images
    public Movies(String movieName,  String movieposterUrl,double movieRating) {
        this.movieName = movieName;
        this.movieRating = movieRating;
        this.movieposterUrl = movieposterUrl;
    }

    //This Constructor is used to get the movie name, run time, genre and it's image for Displaying on next pages
    public Movies(String movieTitle, String movieRunTime, String movieGenre, String movieImgUrl) {
        this.movieTitle = movieTitle;
        this.movieRunTime = movieRunTime;
        this.movieGenre = movieGenre;
        this.movieImgUrl = movieImgUrl;
    }


    //It's returning the name of the movie
    public String getMovieName() {
        return movieName;
    }

    //It's returning the rating of the movie
    public double getMovieRating() {
        return movieRating;
    }

    //It's returning the poster of the movie
    public String getMovieposterUrl() {
        return movieposterUrl;
    }

    //It's returning the upcoming movie name
    public String getupcomingMovieName() {
        return movieName;
    }

    //It's returning the upcoming movie poster
    public String getupcomingMovieUrl() {
        return movieposterUrl;
    }

    //It's returning the title of the movie
    public String getMovieTitle() {
        return movieTitle;
    }

    //It's returning the run time of the movie
    public String getMovieRunTime() {
        return movieRunTime;
    }

    //It's returning the genre of the movie
    public String getMovieGenre() {
        return movieGenre;
    }

    //It's returning the image of the movie
    public String getMovieImgUrl() {
        return movieImgUrl;
    }
}
