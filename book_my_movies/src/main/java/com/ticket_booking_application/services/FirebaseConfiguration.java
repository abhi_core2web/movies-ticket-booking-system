package com.ticket_booking_application.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.ticket_booking_application.Controller.Login;
import com.ticket_booking_application.DashBoard.AppState;


public class FirebaseConfiguration {

    
    private static Firestore db;
    static {
        try {
            initializeFirebase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void initializeFirebase() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("src/main/resources/fx-firestore.json");
        @SuppressWarnings("deprecation")
        FirebaseOptions options = new FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(serviceAccount))
            .build();
        FirebaseApp.initializeApp(options);
        db = FirestoreClient.getFirestore();
    }

    public void addData(String collection, String document, Map<String, Object> data) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection(collection).document(document);
        ApiFuture<WriteResult> result = docRef.set(data);
        result.get();
    }

    public void getData(String collection ,String doc) throws ExecutionException, InterruptedException {
        try {
            DocumentReference docRef = db.collection(collection).document(doc);
            ApiFuture<DocumentSnapshot> snapShot = docRef.get();
            DocumentSnapshot docSnap = snapShot.get();
    
            if (docSnap.exists()) {
              
                String userFullName = docSnap.getString("name");
                String username = docSnap.getString("username");

                AppState.getInstance().setLoggedInUserFullName(userFullName);
                AppState.getInstance().setLoggedInUsername(username);

            } else {
                System.out.println("No such document");
            }
        } catch (Exception e) {
            System.out.println("Error fetching document: " + e.getMessage());
        }
        // System.out.println(userFullName);
    }
    
    
    public boolean authenticateUser(String collection,String username, String password) throws ExecutionException, InterruptedException {
        if (username == null || username.trim().isEmpty()) {
            throw new IllegalArgumentException("'username' must be a non-empty String");
        }
        DocumentSnapshot document = db.collection(collection).document("credential").get().get();
        if (document.exists()) {
            String storedPassword = document.getString("password");
            return password.equals(storedPassword);
        }
        return false;
    }

    public void updateData(String collectionName, String documentId, Map<String,Object> updatedData) throws ExecutionException, InterruptedException {
        // Reference to the Firestore collection
        CollectionReference collection = db.collection(collectionName);

        // Reference to the document to update
        DocumentReference docRef = collection.document(documentId);

        ApiFuture<WriteResult> future = docRef.set(updatedData, SetOptions.merge());

        // Wait for the update to complete
        future.get();
    }
}
