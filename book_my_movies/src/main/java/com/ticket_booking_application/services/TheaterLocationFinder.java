package com.ticket_booking_application.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

public class TheaterLocationFinder {

    private static final String LOCATIONIQ_API_KEY = "pk.1230f9cfb29cb6c01351aed78a20654b";
    private static final String LOCATIONIQ_BASE_URL = "https://us1.locationiq.com/v1/";

    public String[] retrieveLocationCoordinate(String location) {
        String[] coordinate = new String[2];
        try {
            String urlString = LOCATIONIQ_BASE_URL + "search.php?key=" + LOCATIONIQ_API_KEY + "&q=" + location + "&format=json";
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
    
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                    StringBuilder response = new StringBuilder();
                    String inputLine;
                    while ((inputLine = br.readLine()) != null) {
                        response.append(inputLine);
                    }
                    JSONArray jsonArray = new JSONArray(response.toString());
                    if (jsonArray.length() > 0) {
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        coordinate[0] = jsonObject.getString("lat");
                        coordinate[1] = jsonObject.getString("lon");
                    } else {
                        System.out.println("No results found for the given location.");
                    }
                }
            } else {
                System.out.println("GET request failed. Response Code: " + responseCode);
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
        return coordinate[0] != null && coordinate[1] != null ? coordinate : null;
    }

    public String[] retrieveTheatreLocation(String lat, String lon) {
        String[] theaterList = new String[10];
        try {
            String urlString = LOCATIONIQ_BASE_URL + "nearby.php?key=" + LOCATIONIQ_API_KEY + "&lat=" + lat + "&lon=" + lon
                    + "&tag=cinema&radius=5000&format=json";
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                    StringBuilder content = new StringBuilder();
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        content.append(inputLine);
                    }
                    JSONArray jsonArray = new JSONArray(content.toString());
                    int length = Math.min(jsonArray.length(), theaterList.length);
                    for (int i = 0; i < length; i++) {
                        JSONObject theater = jsonArray.getJSONObject(i);
                        theaterList[i] = theater.getString("name");
                    }
                }
            } else {
                System.out.println("GET request failed. Response Code: " + responseCode);
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
        }
        return theaterList[0] != null ? theaterList : null;
    }
}
