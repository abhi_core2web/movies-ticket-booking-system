package com.ticket_booking_application.services;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class ImageLoader {
    private static final String BUCKET_NAME = "java-fx-fire-store.appspot.com"; // Remove "gs://" from the bucket name

    public static String retrieveImage(String fileName) {
        try {
            // Correct the path to the JSON file and use GoogleCredentials for authentication
            FileInputStream serviceAccount = new FileInputStream("F:\\maven\\Storage_firebase\\storage\\src\\main\\resources\\fx-firestore.json");
            Storage storage = StorageOptions.newBuilder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build()
                    .getService();

            BlobId blobId = BlobId.of(BUCKET_NAME, fileName);
            Blob blob = storage.get(blobId);

            if (blob == null || !blob.exists()) {
                throw new IOException("No such object exists");
            }

            byte[] data = blob.getContent();

            // Encode the image to Base64 string
            return Base64.getEncoder().encodeToString(data);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
