package com.ticket_booking_application.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import javafx.scene.image.Image;

public class Bit_APIFetchMovies {
    
    
    private static final String tmdb_API_KEY = "57f638b398daff262c087ac428acb31c";
    private static final String tmdb_API_URL = "https://api.themoviedb.org/3/movie/";

    private static final String API_KEY = "18c3789c";
    private static final String BASE_URL = "http://www.omdbapi.com/?";

    public static List<Movies> moviesList; // Renamed bit_moviesList to moviesList

    /*
     * This method is resposnible for fetching movies from the TMDB API 
     * by sending an HTTP GET request to the specifiead URL.
     * This url is constructed using the category and language parameters.
     * Then Establish a connection to the URL and send a GET request.
     * Response code is Parse the JSON response and extract the movie details.
     * Then JSON Object pass to the JSON Array and iterate through the results array.
     * Then Extract the movie name, rating, and poster URL from each movie object.
     * Then Create a new Movies object with the extracted details and add it to the moviesList.
     * Finally, Return the moviesList.
     * 
     * @auth : Abhishek Bhosale
     */
    public void fetchMovies(String category, String language, int numberOfMovies) {
        try {
            // Clear existing movies list
            moviesList.clear();

            // Fetching first 3 pages (each page contains 10 results)
            for (int page = 1; page <= 3; page++) {
                String urlStr = BASE_URL + "apikey=" + API_KEY + "&type=movie&s=" + category + "&page=" + page + "&language=" + language;
                URL url = new URL(urlStr);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }

                in.close();
                conn.disconnect();

                JSONObject moviesJson = new JSONObject(content.toString());
                JSONArray moviesArray = moviesJson.getJSONArray("Search");

                for (int j = 0; j < moviesArray.length(); j++) {
                    JSONObject movieJson = moviesArray.getJSONObject(j);
                    String title = movieJson.getString("Title");
                    String posterUrl = movieJson.getString("Poster");
                    float randomRating = generateRandomRating(7.1f,9.2f); // Generate random rating

                    Movies movie = new Movies(title, posterUrl, randomRating);
                    moviesList.add(movie);

                    // Break loop if fetched enough movies as per numberOfMovies
                    if (moviesList.size() >= numberOfMovies) {
                        return;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Image getImageOrDefault(String url) {
        try {
            return new Image(url, true);
        } catch (Exception e) {
            return new Image("https://via.placeholder.com/100x150.png?text=No+Image");
        }
    }

    private float generateRandomRating(float min, float max) {
        Random r = new Random();
        return min + (max - min) * r.nextFloat();
    }
    
    public static List<Movies> getMoviesList() {
        return moviesList;
    }

    public void bit_hitAPIForUpcomingMovies(String language) {
        moviesList = new ArrayList<>(); // Initialize moviesList here
        String apiUrl = tmdb_API_URL + "top_rated";
        String apiKeyParam = "api_key=" + tmdb_API_KEY;
        String languageParam = "language=" + language;
    
        String urlStr = apiUrl + "?" + apiKeyParam + "&" + languageParam;
    

        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();
            if(responseCode==HttpURLConnection.HTTP_OK){
                StringBuilder response = new StringBuilder();
                try(BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))){
                    String line;
                    while((line = reader.readLine()) != null){
                        response.append(line);
                    }
                }

                JSONObject jsonObject = new JSONObject(response.toString());
                JSONArray results = jsonObject.getJSONArray("results");

                for(int i = 0; i < results.length(); i++){
                    JSONObject movie = results.getJSONObject(i);
                    String movieName = movie.getString("title");
                    String moviePosterUrl = movie.getString("poster_path");

                    Movies movieObj = new Movies(moviePosterUrl,movieName);
                    moviesList.add(movieObj);
                }

            } else {
                System.out.println("Error: HTTP response code " + responseCode);
            }
        }catch(Exception e){
            System.out.println("Error fetching movies: " + e.getMessage());
        }
    }
}
