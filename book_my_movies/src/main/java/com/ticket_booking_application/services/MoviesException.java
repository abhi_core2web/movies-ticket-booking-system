package com.ticket_booking_application.services;

public class MoviesException extends Exception{
    
    public MoviesException(String message){
        super(message);
    }
}
