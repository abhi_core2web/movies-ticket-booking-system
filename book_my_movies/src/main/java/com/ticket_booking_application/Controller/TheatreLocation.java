package com.ticket_booking_application.Controller;

import java.time.LocalTime;
import java.util.Random;

import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.Movies;
import com.ticket_booking_application.services.MoviesException;
import com.ticket_booking_application.services.TheaterLocationFinder;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.BorderPane;

public class TheatreLocation extends ProjectNavBar {

    Landing landing;
    BorderPane root;

    VBox bit_theatreLocationBox;
    Button bit_navLogin;
    Label bit_navProfileText;
    Label bit_navLocationText;
    String bit_movieName;
    Accordion bit_theatreNameAccordian;
    
    int normalSeats;
    int premiumSeats;
    int premiumSeatPrize;
    int normalSeatPrize;
    String bit_movieTime;
    Label bit_movieTitle;
    Label bit_theatreName;
    TilePane bit_premiumSeats;
    TilePane bit_normalSeats;
    Label bit_premiumSeatPrize;
    Label bit_theatreCapacityLabel;
    Label bit_normalSeatPrize;
    Label bit_movieTimeLabel;
    Label bit_primiumTicketCount;
    Label bit_normalTicketCount;
    Label bit_totalTicket;
    Label bit_totalAmount;
    Button bit_paymentButton;
    Label bit_dateLabel;
    Label bit_theatreLabel;
    private int currentPageNumber = 4;


    String theatreName;

    Movies movies = new Movies();
    Random random = new Random();

    public TheatreLocation(Landing landing) {
        this.landing = landing;
        createThertreLocation();
    }

    private void createThertreLocation() {
        root = new BorderPane();
        HBox bit_theatreLocationMainBox = new HBox(120, bit_createLeftMainBody(), bit_createRightMainBody());
        bit_theatreLocationMainBox.setPadding(new Insets(30, 60, 30, 60));
        bit_theatreLocationMainBox.setStyle("-fx-background-color:#a8baac");
        root.setTop(createNavBar());
        root.setCenter(bit_theatreLocationMainBox);
        bit_setTheatreLocation();
    }
    private VBox bit_createLeftMainBody() {

        bit_movieTitle = new Label();
        bit_movieTitle.setStyle("-fx-font-size: 30; -fx-font-weight: bold; -fx-text-fill: Black;");
        // Label bit_monthYearLabel = new Label("Month Year:\nJuly 2024");
        // bit_monthYearLabel.setStyle("-fx-font-size: 20; -fx-font-weight: bold;
        // -fx-text-fill: Black;");
        HBox bit_movieTitleDate = new HBox(200, bit_movieTitle);

        HBox bit_dateForMoviesBox = new HBox();

        bit_dateForMoviesBox.setSpacing(10);
        bit_dateForMoviesBox.setPadding(new Insets(10, 10, 10, 10));
        String Date = AppState.getInstance().getDate();
        // int date = Integer.parseInt(Date.split(" ")[1]);    // Extracting date from the string
        int date = 8;
        for (int i = date; i < (date + 7); i++) {
            Button bit_dateForMovies = new Button(i + "");
            bit_dateForMovies.setPrefSize(100, 100);
            bit_dateForMovies.setStyle("-fx-background-color: orange; -fx-text-fill: white; -fx-font-size: 25px;  -fx-border-width: 1; -fx-border-color: black; -fx-border-radius: 5; -fx-font-weight: bold;");
            bit_dateForMovies.setCursor(Cursor.HAND);
            bit_dateForMovies.setOnAction(e -> {
                bit_setTheatreLocation();
            });
            bit_dateForMoviesBox.getChildren().add(bit_dateForMovies);
        }

        bit_theatreNameAccordian = new Accordion();
        bit_theatreNameAccordian.setPrefWidth(400);
        bit_theatreNameAccordian.setPrefHeight(850);
        bit_theatreLabel = new Label();

        bit_theatreLocationBox = new VBox(bit_theatreNameAccordian, bit_theatreLabel);
        VBox bit_theatreLocationLeftBox = new VBox(30, bit_movieTitleDate, bit_dateForMoviesBox,
                bit_theatreLocationBox);
        bit_theatreLocationLeftBox.setAlignment(Pos.CENTER);
        bit_theatreLocationLeftBox.setMaxHeight(850);
        bit_theatreLocationLeftBox.setPadding(new Insets(30));

        return bit_theatreLocationLeftBox;
    }

    private VBox bit_createRightMainBody() {
        // Create rounded rectangle for screen stack
        Rectangle bit_roundedRect = new Rectangle(650, 90);
        bit_roundedRect.setArcWidth(20);
        bit_roundedRect.setArcHeight(20);
        bit_roundedRect.setStyle("-fx-fill: black; -fx-opacity: 0.8;");
    
        // Label for theatre name
        bit_theatreName = new Label();
        bit_theatreName.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_theatreName.setPadding(new Insets(0, 10, 55, 10));
    
        // Screen label
        Label bit_screenLabel = new Label("Screen");
        bit_screenLabel.setStyle("-fx-font-size: 30; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_screenLabel.setPadding(new Insets(55, 10, 0, 10));
        
        // StackPane for screen stack
        StackPane bit_screenStack = new StackPane(bit_roundedRect, bit_theatreName, bit_screenLabel);
        bit_screenStack.setAlignment(Pos.CENTER);
    
        // Normal seats label
        Label bit_normalSeatLabel = new Label("Normal Seats");
        bit_normalSeatLabel.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_normalSeats = new TilePane();
        bit_normalSeats.setAlignment(Pos.CENTER);
        bit_normalSeats.setPrefColumns(12);
        bit_normalSeats.setPrefRows(6);
        bit_normalSeats.setHgap(8);
        bit_normalSeats.setVgap(5);
        
        // Normal seats VBox
        VBox bit_normalSeatStatusBox = new VBox(10, bit_normalSeatLabel, bit_normalSeats);
        bit_normalSeatStatusBox.setAlignment(Pos.CENTER);
        bit_normalSeatStatusBox.setPadding(new Insets(10, 10, 10, 10));
        bit_normalSeatStatusBox.setStyle("-fx-background-color: rgba(255, 255, 255, 0.1); -fx-background-radius: 15;");
    
        // Premium seats label
        Label bit_premiumSeatLabel = new Label("Premium Seats");
        bit_premiumSeatLabel.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_premiumSeats = new TilePane();
        bit_premiumSeats.setAlignment(Pos.CENTER);
        bit_premiumSeats.setPrefColumns(10);
        bit_premiumSeats.setPrefRows(3);
        bit_premiumSeats.setHgap(5);
        bit_premiumSeats.setVgap(5);
        
        // Premium seats VBox
        VBox bit_premiumSeatStatusBox = new VBox(10, bit_premiumSeatLabel, bit_premiumSeats);
        bit_premiumSeatStatusBox.setAlignment(Pos.CENTER);
        bit_premiumSeatStatusBox.setPadding(new Insets(10, 10, 10, 10));
        bit_premiumSeatStatusBox.setStyle("-fx-background-color: rgba(255, 255, 255, 0.1); -fx-background-radius: 15;");
    
        // VBox for screen and seats
        VBox bit_screenSeats = new VBox(30, bit_screenStack, bit_normalSeatStatusBox, bit_premiumSeatStatusBox);
        
        // Premium and normal seat price labels
        bit_premiumSeatPrize = new Label();
        bit_premiumSeatPrize.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_normalSeatPrize = new Label();
        bit_normalSeatPrize.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        
        // Booked and available seats labels
        Label bit_bookedSeats = new Label("Booked Seats");
        bit_bookedSeats.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        Rectangle bit_bookedSeatsRect = new Rectangle(50, 50);
        bit_bookedSeatsRect.setFill(Color.RED);
        Label bit_availableSeats = new Label("Available Seats");
        bit_availableSeats.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        Rectangle bit_availableSeatsRect = new Rectangle(50, 50);
        bit_availableSeatsRect.setFill(Color.GREEN);
    
        // Theatre capacity, movie time, and date labels
        bit_theatreCapacityLabel = new Label();
        bit_theatreCapacityLabel.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_movieTimeLabel = new Label();
        bit_movieTimeLabel.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_dateLabel = new Label();
        bit_dateLabel.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
    
        // VBox for left labels
        VBox bit_leftSomeLaBox = new VBox(20, bit_premiumSeatPrize, bit_normalSeatPrize, bit_bookedSeats, bit_bookedSeatsRect, bit_availableSeats, bit_availableSeatsRect, bit_theatreCapacityLabel, bit_movieTimeLabel, bit_dateLabel);
        bit_leftSomeLaBox.setMinWidth(200);
        bit_leftSomeLaBox.setPadding(new Insets(10, 10, 10, 10));
        bit_leftSomeLaBox.setStyle("-fx-background-color: rgba(255, 255, 255, 0.1); -fx-background-radius: 15;");
        
        // HBox for seat selection
        HBox bit_seatSelectionBox = new HBox(50, bit_leftSomeLaBox, bit_screenSeats);
        bit_seatSelectionBox.setPrefHeight(600);
    
        // Ticket count and total amount labels
        Label bit_ticketCountLabel = new Label("Number of Tickets");
        bit_ticketCountLabel.setStyle("-fx-font-size: 30; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_primiumTicketCount = new Label("Premium: 0");
        bit_primiumTicketCount.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_normalTicketCount = new Label("Normal: 0");
        bit_normalTicketCount.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_totalTicket = new Label("Tickets: 0");
        bit_totalTicket.setStyle("-fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: white;");
        
        // VBox for ticket count
        VBox bit_ticketCountBox = new VBox(10, bit_ticketCountLabel, bit_primiumTicketCount, bit_normalTicketCount, bit_totalTicket);
        bit_ticketCountBox.setPadding(new Insets(10, 10, 10, 30));
        bit_ticketCountBox.setAlignment(Pos.CENTER);
    
        // Total amount label and payment button
        bit_totalAmount = new Label("Total Amount: 0");
        bit_totalAmount.setStyle("-fx-font-size: 30; -fx-font-weight: bold; -fx-text-fill: white;");
        bit_paymentButton = new Button("Proceed to Pay");
        bit_paymentButton.setPrefSize(200, 50);
        bit_paymentButton.setStyle("-fx-background-color: blue; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_paymentButton.setCursor(Cursor.HAND);
        bit_paymentButton.setOnAction(e -> {
            AppState.getInstance().setSubTotal(bit_totalAmount.getText());
            landing.redirectToPayment();
        });
    
        // VBox for total amount and payment button
        VBox bit_totalAmountBox = new VBox(80, bit_totalAmount, bit_paymentButton);
        bit_totalAmountBox.setPadding(new Insets(20, 10, 10, 200));
    
        // HBox for ticket count and total amount
        HBox bit_numberOfTicketBox = new HBox(30, bit_ticketCountBox, bit_totalAmountBox);
        bit_numberOfTicketBox.setPrefHeight(200);
    
        // Main VBox for right main body
        VBox bit_theatreRightSeatBox = new VBox(48, bit_seatSelectionBox, bit_numberOfTicketBox);
        bit_theatreRightSeatBox.setPrefWidth(850);
        bit_theatreRightSeatBox.setMaxHeight(850);
        bit_theatreRightSeatBox.setStyle(
            "-fx-background-color: #" + // Gradient background
            "-fx-background-radius: 25;" + // Rounded corners for background
            "-fx-border-color: white;" + // White border color
            "-fx-border-width: 3px;" + // Border width
            "-fx-border-radius: 25;" + // Rounded corners for border
            "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.25), 40, 0.5, 0.0, 1.5);" // Soft drop shadow effect
        );
    
        return bit_theatreRightSeatBox;
    }
    
    public void bit_setTheatreLocation() {

        TheaterLocationFinder theatreLocation = new TheaterLocationFinder();
        
        String[] location = theatreLocation.retrieveLocationCoordinate(AppState.getInstance().getLocation());
        

        if (location != null) {
            String[] theatreList = theatreLocation.retrieveTheatreLocation(location[0], location[1]);

            try{
                if(theatreList == null){
                    throw new MoviesException("थिएटर सापडला नाही.");
                }else{
                    bit_theatreNameAccordian.getPanes().clear();
                
                    for (int i = 0; i < theatreList.length; i++) {
                        if (theatreList[i] != null) {
    
                            TilePane bit_showTimeBox = new TilePane();
                            bit_showTimeBox.setPrefColumns(3);
                            bit_showTimeBox.setPrefRows(2);
                            bit_showTimeBox.setHgap(30);
                            bit_showTimeBox.setVgap(30);
                            bit_showTimeBox.setAlignment(Pos.CENTER);
                            for(int j=1; j<=5; j++){
                                Button bit_screenButton = new Button();
                                bit_screenButton.setPrefSize(150, 50);
                                bit_screenButton.setStyle("-fx-background-color: orange; -fx-text-fill: white; -fx-font-size: 20; -fx-border-width: 1; -fx-border-color: black; -fx-border-radius: 5; -fx-font-weight: bold;");
                                bit_screenButton.setCursor(Cursor.HAND);
                                bit_screenButton.setOnAction(e -> {
                                    bit_movieTime = bit_screenButton.getText();
                                    AppState.getInstance().setMovieTime(bit_movieTime);
                                    setTimeForMovie();
                                });
                    
                                int hour = random.nextInt(24);
                                int minute = random.nextInt(60);
                                LocalTime randomTime = LocalTime.of(hour, minute);
                                bit_screenButton.setText(randomTime.toString());
                                bit_showTimeBox.getChildren().add(bit_screenButton);
                            }
    
                            TitledPane bit_theatrePane = new TitledPane();
    
                            bit_theatrePane.setContent(bit_showTimeBox);
                            bit_theatrePane.setText(theatreList[i]);
                            bit_theatrePane.setPrefWidth(400);
                            bit_theatrePane.setPrefHeight(100);
                            bit_theatrePane.setStyle("-fx-background-color: green; -fx-text-fill: blue; -fx-font-weight: bold; -fx-font-size: 20;");
                            bit_theatrePane.setCursor(Cursor.HAND);
                            bit_theatrePane.setOnMouseClicked(e -> {
                                theatreName = bit_theatrePane.getText();
                                normalSeats = 10 + random.nextInt(21); 
                                premiumSeats = 8 + random.nextInt(13); 
                                premiumSeatPrize = 300+ random.nextInt(100);
                                normalSeatPrize = 200+ random.nextInt(100);
                                bit_dynamicUpdate();
                            });
    
                            bit_theatreNameAccordian.getPanes().add(bit_theatrePane);
                        }
                    }
                }
            }catch(MoviesException e){
                bit_theatreLabel.setText(e.getMessage());
                bit_theatreLabel.setStyle("-fx-font-size: 30; -fx-font-weight: bold; -fx-text-fill: red;");
            }
        }  
    }

    private void bit_dynamicUpdate() {
        if(theatreName == AppState.getInstance().getTheatreName()){
            return;
        }else{
            clearAmount();
            AppState.getInstance().setTheatreName(theatreName);
            bit_movieTitle.setText("Title: "+AppState.getInstance().getMovieName());
            bit_theatreName.setText(AppState.getInstance().getTheatreName());
            AppState.getInstance().setTheatreName(theatreName);
            bit_premiumSeatPrize.setText("Premium cost: \n"+premiumSeatPrize + " Rs");
            bit_normalSeatPrize.setText("Normal cost: \n"+normalSeatPrize + " Rs");
            bit_theatreCapacityLabel.setText("Total Capacity: \n"+(normalSeats+premiumSeats));

            bit_normalSeats.getChildren().clear();

            
            for (int i = 0; i < normalSeats; i++) {
                Rectangle bit_normalSeat = new Rectangle(50, 50);
                bit_normalSeat.setFill(Color.GREEN);
                bit_normalSeat.setCursor(Cursor.HAND);

                Label seatNumber = new Label(String.valueOf("A"+i + 1));
                StackPane seat = new StackPane();
                seat.getChildren().addAll(bit_normalSeat, seatNumber);

                bit_normalSeats.getChildren().add(seat);

                bit_normalSeat.setOnMouseClicked(e -> {
                    if (bit_normalSeat.getFill() == Color.GREEN) {
                        updateAmount("Addition", normalSeatPrize);
                        bit_normalSeat.setFill(Color.RED);
                    } else if (bit_normalSeat.getFill() == Color.RED) {
                        updateAmount("Subtraction", normalSeatPrize);
                        bit_normalSeat.setFill(Color.GREEN);
                    }
                });
            }

            bit_premiumSeats.getChildren().clear();

            for (int i = 0; i < premiumSeats; i++) {
                Rectangle bit_premiumSeat = new Rectangle(50, 50);
                bit_premiumSeat.setFill(Color.GREEN);
                bit_premiumSeat.setCursor(Cursor.HAND);
                Label seatNumber = new Label(String.valueOf("B"+i + 1));
                StackPane seat = new StackPane();
                seat.getChildren().addAll(bit_premiumSeat, seatNumber);

                bit_premiumSeats.getChildren().add(seat);

                bit_premiumSeat.setOnMouseClicked(e -> {
                    if (bit_premiumSeat.getFill() == Color.GREEN) {
                        updateAmount("Addition", premiumSeatPrize);
                        bit_premiumSeat.setFill(Color.RED);
                    } else if (bit_premiumSeat.getFill() == Color.RED) {
                        updateAmount("Subtraction", premiumSeatPrize);
                        bit_premiumSeat.setFill(Color.GREEN);

                    }
                });
            }
        }
    }

    public void clearAmount() {
        bit_primiumTicketCount.setText("Premium: 0");
        bit_normalTicketCount.setText("Normal: 0");
        bit_totalTicket.setText("Tickets: 0");
        bit_totalAmount.setText("Total Amount: 0");
    }

    private void updateAmount(String operation, int prize) {
        
        if(operation == "Addition"){
            int totalTicket = Integer.parseInt(bit_totalTicket.getText().split(": ")[1]);
            totalTicket++;
            bit_totalTicket.setText("Tickets: "+totalTicket);
            
            int totalPremiumTicket = Integer.parseInt(bit_primiumTicketCount.getText().split(": ")[1]);
            int totalNormalTicket = Integer.parseInt(bit_normalTicketCount.getText().split(": ")[1]);
            
            if(prize == premiumSeatPrize){
                totalPremiumTicket++;
                bit_primiumTicketCount.setText("Premium: "+totalPremiumTicket);
            }else if(prize == normalSeatPrize){
                totalNormalTicket++;
                bit_normalTicketCount.setText("Normal: "+totalNormalTicket);
            }

            int totalAmount = Integer.parseInt(bit_totalAmount.getText().split(": ")[1]);
            totalAmount += prize;
            bit_totalAmount.setText("Total Amount: "+totalAmount);
        }else if(operation == "Subtraction"){
            int totalTicket = Integer.parseInt(bit_totalTicket.getText().split(": ")[1]);
            totalTicket--;
            bit_totalTicket.setText("Tickets: "+totalTicket);
            int totalAmount = Integer.parseInt(bit_totalAmount.getText().split(": ")[1]);
            totalAmount -= prize;
            bit_totalAmount.setText("Total Amount: "+totalAmount);
        }
    }

    private void setTimeForMovie() {
        bit_movieTimeLabel.setText("Movie Time: \n"+AppState.getInstance().getMovieTime());
        bit_dateLabel.setText("Date: \n"+AppState.getInstance().getDate());
    }
    
    public BorderPane getTheatreLocationScene() {
        return root;
    }
}
