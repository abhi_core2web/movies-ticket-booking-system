package com.ticket_booking_application.Controller;

import org.json.JSONObject;

import com.google.type.Color;
import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.Bit_APISearchMovie;
import com.ticket_booking_application.services.Movies;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class MovieInfo extends ProjectNavBar {

    HBox bit_borderPane;
    Landing landing;

    HBox bit_header;
    HBox bit_mainInfoBox;
    // Button for login
    Button bit_navLogin;
    // Label for profile text
    Label bit_navProfileText;
    // Label for location text
    Label bit_navLocationText;
    String movieName;
    private int currentPageNumber = 3;

    Movies movies = new Movies();

    public MovieInfo(Landing landing) {
        this.landing = landing;
        createMovieInfoPage();
    }

    public HBox createMovieInfoPage() {

        BorderPane bit_borderPaneAlign = new BorderPane();
        bit_header = createNavBar();
        bit_mainInfoBox = movieInfoSection();
        bit_mainInfoBox.setStyle("-fx-background-color: #088395;");

        bit_borderPaneAlign.setTop(bit_header);
        bit_borderPaneAlign.setCenter(bit_mainInfoBox);
        bit_borderPane = new HBox(bit_borderPaneAlign);
        bit_borderPane.setStyle("-fx-background-color: #088395;");
        bit_borderPane.setAlignment(Pos.CENTER);
        bit_borderPane.setPadding(new Insets(20, 0, 0, 0));
        return bit_borderPane;
    }

    public HBox movieInfoSection() {

        Label bit_movieInformation = new Label("Movie Information");
        HBox bit_mainInfoBox = new HBox(bit_movieInformation);
        bit_mainInfoBox.setAlignment(Pos.CENTER);
        bit_mainInfoBox.setPadding(new Insets(40,20,40,20));
        return bit_mainInfoBox;
        

    }

    public void bit_initTheInfoPage(String movieName) {

        JSONObject bit_movieInfo = Bit_APISearchMovie.bit_fetchMovies(movieName);

        if (bit_movieInfo == null) {
            System.out.println("Movie info is null");
            return;
        }

        String title = bit_movieInfo.optString("Title", "N/A");
        String language = bit_movieInfo.optString("Language", "N/A");
        String duration = bit_movieInfo.optString("Runtime", "N/A");
        String genre = bit_movieInfo.optString("Genre", "N/A");
        String releaseDate = bit_movieInfo.optString("Released", "N/A");
        String boxOffice = bit_movieInfo.optString("BoxOffice", "N/A");
        String plot = bit_movieInfo.optString("Plot", "N/A");
        String poster = bit_movieInfo.optString("Poster", "N/A");
        String director = bit_movieInfo.optString("Director", "N/A");
        String writer = bit_movieInfo.optString("Writer", "N/A");
        String actors = bit_movieInfo.optString("Actors", "N/A");
        String imdbRating = bit_movieInfo.optString("imdbRating", "N/A");
        String imdbVotes = bit_movieInfo.optString("imdbVotes", "N/A");

        bit_createMovieInfoBox(title, language, duration, genre, releaseDate, boxOffice, plot, poster, director, writer,
                actors, imdbRating, imdbVotes);
    }

    public void bit_createMovieInfoBox(String title, String language, String runTime, String genre, String release_date,
                                   String boxOffice, String Info, String imgUrl, String director, String writer, String cast,
                                   String imdbRating, String imdbVotes) {
    // Check if bit_mainInfoBox is null
    if (bit_mainInfoBox == null) {
        System.out.println("bit_mainInfoBox is null");
        return; // Exit the method if bit_mainInfoBox is null
    }

    // Clear the main info box
    bit_mainInfoBox.getChildren().clear();

    // Create the movie image
    try {
        Image bit_movieImage = new Image(imgUrl);
        ImageView bit_movieImageView = new ImageView(bit_movieImage);
        bit_movieImageView.setPreserveRatio(true);
        bit_movieImageView.setFitHeight(520);
        bit_movieImageView.setFitWidth(380);
        bit_movieImageView.setStyle(" -fx-background-radius: 15; -fx-border: white solid 6px;");

        // Create the movie rating and votes
        Label bit_movieRating = new Label("⭐" + imdbRating);
        bit_movieRating.setStyle("-fx-font-size: 30; -fx-text-fill: white; -fx-font-weight: bold;");
        Label bit_movieVotes = new Label(imdbVotes + " Votes");
        bit_movieVotes.setStyle("-fx-font-size: 30; -fx-text-fill: white;");
        HBox bit_movieRatingBox = new HBox(15, bit_movieRating, bit_movieVotes);

        // Create the movie info section
        VBox bit_infoLeftBox = new VBox(40, bit_movieImageView, bit_movieRatingBox);
        bit_infoLeftBox.setPadding(new Insets(60, 0, 0, 60));

        // Movie title and language
        Label bit_movieTitle = new Label("Title: " + title);
        bit_movieTitle.setStyle("-fx-font-size: 50; -fx-text-fill: white; -fx-font-weight: bold;");
        bit_movieTitle.setWrapText(true);
        bit_movieTitle.setMinWidth(500);

        Button bit_movieLanguage = new Button("Language: " + language);
        bit_movieLanguage.setStyle(
                "-fx-background-color: white; -fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 18;");
        VBox bit_movieTitleLang = new VBox(20, bit_movieTitle, bit_movieLanguage);

        // Movie details
        Label bit_movieDuration = new Label("Duration: " + runTime);
        bit_movieDuration.setStyle("-fx-font-size: 20; -fx-text-fill: white;");
        Label bit_movieGenre = new Label("Genre: " + genre);
        bit_movieGenre.setStyle("-fx-font-size: 20; -fx-text-fill: white;");
        Label bit_movieReleaseDate = new Label("Release Date: " + release_date);
        bit_movieReleaseDate.setStyle("-fx-font-size: 20; -fx-text-fill: white;");
        Label bit_movieBoxOffice = new Label("Box Office: " + boxOffice);
        bit_movieBoxOffice.setStyle("-fx-font-size: 20; -fx-text-fill: white;");

        VBox bit_movieDetail = new VBox(10, bit_movieDuration, bit_movieGenre, bit_movieReleaseDate, bit_movieBoxOffice);

        // About the movie
        Label bit_labelAboutMovie = new Label("About the movie");
        bit_labelAboutMovie.setStyle("-fx-font-size: 30; -fx-text-fill: white; -fx-font-weight: bold;");
        Text bit_aboutMovieText = new Text(Info);
        bit_aboutMovieText.setStyle("-fx-font-size: 20; -fx-text-fill: orange; -fx-font-weight: bold;");
        bit_aboutMovieText.setFill(javafx.scene.paint.Color.WHITE);
        bit_aboutMovieText.setWrappingWidth(500); // Adjust wrapping width to fit screen
        VBox bit_aboutMovie = new VBox(10, bit_labelAboutMovie, bit_aboutMovieText);

        // Create right info box
        VBox bit_infoRightBox = new VBox(20, bit_movieTitleLang, bit_movieDetail, bit_aboutMovie);
        bit_infoRightBox.setPadding(new Insets(60, 0, 0, 200));

        Label bit_directorLabel = new Label("Director: ");
        bit_directorLabel.setStyle("-fx-font-size: 30; -fx-text-fill: white;");
        Label bit_directorName = new Label("⭐ " + director);
        bit_directorName.setStyle("-fx-font-size: 20; -fx-text-fill: white;");

        Label bit_writerLabel = new Label("Writer: ");
        bit_writerLabel.setStyle("-fx-font-size: 30; -fx-text-fill: white;");
        Label bit_writerName = new Label("⭐ " + writer);
        bit_writerName.setStyle("-fx-font-size: 20; -fx-text-fill: white;");

        Label bit_castLabel = new Label("Cast: ");
        bit_castLabel.setStyle("-fx-font-size: 30; -fx-text-fill: white;");
        Label bit_castName = new Label("⭐ " + cast);
        bit_castName.setStyle("-fx-font-size: 20; -fx-text-fill: white;");
        bit_castName.setWrapText(true);
        bit_castName.setMaxWidth(300);

        VBox bit_castRightBox = new VBox(20, bit_directorLabel, bit_directorName, bit_writerLabel, bit_writerName, bit_castLabel, bit_castName);
        bit_castRightBox.setPadding(new Insets(80, 0, 0, 0));

        HBox bit_rightPartBox = new HBox(100, bit_infoRightBox, bit_castRightBox);

        Button bit_bookTicketsButton = new Button("Book tickets");
        bit_bookTicketsButton.setMinHeight(60);
        bit_bookTicketsButton.setMinWidth(350);
        bit_bookTicketsButton.setFont(new Font(21));
        bit_bookTicketsButton.setAlignment(Pos.CENTER);
        bit_bookTicketsButton.setStyle(
                "-fx-text-fill: white; -fx-background-color: red; -fx-background-radius: 7; -fx-font-weight: bold; -fx-font-size: 25;");
        bit_bookTicketsButton.setCursor(Cursor.HAND);
        bit_bookTicketsButton.setOnAction(e -> {
            AppState.getInstance().setMovieName(title);
            AppState.getInstance().setMovieLanguage(language);
            AppState.getInstance().setMovieRunTime(runTime);
            AppState.getInstance().setMoviePoster(imgUrl);

            landing.redirectToTheater();
        });

        HBox bit_bookButtBox = new HBox(bit_bookTicketsButton);
        bit_bookButtBox.setPadding(new Insets(0, 0, 150, 900));

        // Align info boxes
        BorderPane bit_aligninfoBoxes = new BorderPane();
        bit_aligninfoBoxes.setLeft(bit_infoLeftBox);
        bit_aligninfoBoxes.setRight(bit_rightPartBox);
        bit_aligninfoBoxes.setBottom(bit_bookButtBox);

        HBox bit_movieInfoBox1 = new HBox(bit_aligninfoBoxes);
        bit_movieInfoBox1.setLayoutX(100);
        bit_movieInfoBox1.setStyle(
            "-fx-background-color: #37B7C3;" +
            "-fx-background-radius: 25;" +
            "-fx-border-color: white;" +
            "-fx-border-width: 3px;" +
            "-fx-border-radius: 25;" +
            "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.25), 40, 0.5, 0.0, 1.5);"
        );

        bit_movieInfoBox1.setAlignment(Pos.CENTER);
        bit_movieInfoBox1.setMinHeight(300);
        bit_movieInfoBox1.setMinWidth(600);

        // Add created info box to main info box
        bit_mainInfoBox.getChildren().add(bit_movieInfoBox1);

    } catch (Exception e) {
        System.out.println("An error occurred: " + e.getMessage());
        e.printStackTrace();
    }
}

    public HBox getMovieScene() {
        return bit_borderPane;
    }
}
