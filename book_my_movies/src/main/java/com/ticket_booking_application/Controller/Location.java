package com.ticket_booking_application.Controller;

import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.Bit_APIFetchNearLocation;
import com.ticket_booking_application.services.MoviesException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class Location  {

    private TilePane bit_nearLocationPane;
    private TilePane bit_popularCites;
    private VBox bit_locationPage; 
    private HBox bit_nearLocationBox;
    public static String LOCATION;

    Landing landing;

    public Location(Landing landing) {
        this.landing = landing;
        AppState.getInstance().setDate("2021-09-01");
        createLocationPage();
    }
    
    private void createLocationPage() {
        Button bit_backButton = new Button("Back");
        bit_backButton.setStyle("-fx-background-color: #102C57; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_backButton.setCursor(Cursor.HAND);
        bit_backButton.setOnAction(e -> {
            landing.redirectToLogin();
        });
        Label selectLabel = new Label("Select City");;
        selectLabel.setStyle("-fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 30;");

        HBox bit_topLeSearch = new HBox(20,bit_backButton,selectLabel);
        // TextField for searching the city
        TextField locationSearchBar = new TextField();
        locationSearchBar.setPromptText("Search for City");
        locationSearchBar.setFocusTraversable(false);
        locationSearchBar.setMinWidth(500);
        locationSearchBar.setMinHeight(50);
        locationSearchBar.setCursor(Cursor.TEXT);
        locationSearchBar.setPadding(new Insets(10, 10, 10, 20));
        locationSearchBar.setFont(Font.font("Arial"));
        locationSearchBar.setStyle(" -fx-border-color: white; -fx-border-width: 3px; -fx-border-radius: 30px; -fx-text-fill: white;");

        // Search icon setup
        Image searchIcon = new Image("images/search.png");
        ImageView searchIconObj = new ImageView(searchIcon);
        searchIconObj.setFitHeight(50);
        searchIconObj.setFitWidth(50);
        searchIconObj.setPreserveRatio(true);
        searchIconObj.setCursor(Cursor.HAND);
        searchIconObj.setOnMouseClicked(e -> {
            LOCATION = locationSearchBar.getText();
           try{
                if(LOCATION == null || LOCATION.trim().isEmpty()){
                    throw new MoviesException("शहर प्रविष्ट केलेले नाही");
                }
                updateDyanamicLocatons();
            }catch(MoviesException ex){
                ProjectNavBar.showDialog("Error","images/warning.png",ex.getMessage());
           }
        });

        // StackPane for search bar and search icon
        StackPane locationSearchIconStack = new StackPane(locationSearchBar, searchIconObj);
        locationSearchBar.setStyle("-fx-background-color: #414958; -fx-background-radius: 30px; -fx-text-fill: white; -fx-font-size: 20; -fx-font-weight: bold;");
        locationSearchIconStack.setAlignment(Pos.CENTER_RIGHT);
        StackPane.setMargin(searchIconObj, new Insets(0, 0, 0, 20));

        HBox bit_topSearch = new HBox(500,bit_topLeSearch,locationSearchIconStack);
        bit_topSearch.setStyle("-fx-background-color: #1679AB; -fx-background-radius: 40px;");    
        bit_topSearch.setPadding(new Insets(20, 0, 20, 0));     
        bit_topSearch.setAlignment(Pos.CENTER);
        bit_topSearch.setMaxHeight(90);
        bit_topSearch.setMaxWidth(1300);

        bit_popularCites = new TilePane();
        bit_popularCites.setPrefColumns(4);
        bit_popularCites.setPrefRows(4);
        bit_popularCites.setHgap(20);
        bit_popularCites.setVgap(20);
        bit_popularCitesStack();

        HBox bit_popularCitesBox = new HBox(bit_popularCites);
        bit_popularCitesBox.setStyle("-fx-background-color: #1679AB; -fx-background-radius: 50px;");

        bit_popularCitesBox.setAlignment(Pos.CENTER);
        bit_popularCitesBox.setMaxWidth(1300);
        bit_popularCitesBox.setPadding(new Insets(30, 0, 30, 0));


        bit_nearLocationPane = new TilePane();
        bit_nearLocationPane.setPrefColumns(8   );
        bit_nearLocationPane.setPrefRows(4);
        bit_nearLocationPane.setHgap(20);
        bit_nearLocationPane.setVgap(20);
        bit_nearLocationPane.setPadding(new Insets(20, 20, 20, 20));
        bit_nearLocationPane.setAlignment(Pos.CENTER);
        bit_nearLocationPane.setStyle("-fx-background-color: #1679AB; -fx-background-radius: 40px;");

        bit_nearLocationBox = new HBox(bit_nearLocationPane);
        bit_nearLocationBox.setVisible(false);
        bit_nearLocationBox.setStyle("-fx-background-color: #6F747D; -fx-background-radius: 40px;");
        bit_nearLocationBox.setAlignment(Pos.CENTER);
        bit_nearLocationBox.setMaxWidth(1300);
        bit_nearLocationBox.setMinHeight(280);



        VBox bit_locationPageStruct = new VBox(60,bit_topSearch, bit_popularCitesBox, bit_nearLocationBox);
        bit_locationPageStruct.setAlignment(Pos.CENTER);
        bit_locationPageStruct.setStyle("-fx-background-color: #102C57; " +
    "-fx-background-image: linear-gradient(to bottom right, #102C57, #1e1e1e);");


        bit_locationPageStruct.setPadding(new Insets(40, 20, 20, 20));

        ScrollPane bit_locationPageScrollPane = new ScrollPane(bit_locationPageStruct);
        bit_locationPageScrollPane.setFitToWidth(true);

        bit_locationPage = new VBox(bit_locationPageScrollPane);

    }
        
    private void bit_popularCitesStack() {
        String bit_nameOfCites[] = new String[]{"Bengaluru","Chandigarh","Chennai","Delhi","Kolkata","Mumbai","Hyderabad","Pune"};
        int i=0;
        for(; i<7 ; i++){
            Label cityLabel = new Label(bit_nameOfCites[i]);
            cityLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; ");
            StackPane.setAlignment(cityLabel, Pos.BOTTOM_CENTER);
            Image cityImage = new Image("images/img"+i+".png");
            ImageView cityImageView = new ImageView(cityImage);
            cityImageView.setFitWidth(250);
            cityImageView.setFitHeight(200);
            cityImageView.setCursor(Cursor.HAND);
            cityImageView.setOnMouseClicked(e->{
                LOCATION = cityLabel.getText();
                updateDyanamicLocatons();

            });
            StackPane cityEntry = new StackPane();
            cityEntry.getChildren().addAll(cityImageView, cityLabel);
            bit_popularCites.getChildren().add(cityEntry);
        }
        Label cityLabel = new Label(bit_nameOfCites[i]);
        cityLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; ");
        StackPane.setAlignment(cityLabel, Pos.BOTTOM_CENTER);
        Image cityImage = new Image("images/img"+i+".jpg");
        ImageView cityImageView = new ImageView(cityImage);
        cityImageView.setFitWidth(250);
        cityImageView.setFitHeight(200);
        cityImageView.setCursor(Cursor.HAND);
        cityImageView.setOnMouseClicked(e->{
            try {
                if(cityLabel.getText() == null){
                    throw new MoviesException("Ciry not Entered");
                }
            }catch(MoviesException ex){
                ProjectNavBar.showDialog("Error","images/warning.png",ex.getMessage());
            }
            LOCATION = cityLabel.getText();
            updateDyanamicLocatons();
        });
        StackPane cityEntry = new StackPane();
        cityEntry.getChildren().addAll(cityImageView, cityLabel);
        bit_popularCites.getChildren().add(cityEntry);
    }

    private void updateDyanamicLocatons(){
        bit_nearLocationBox.setVisible(true);
        String cordinate[] = Bit_APIFetchNearLocation.getCoordinates(LOCATION);
        String bit_nearLocation[] =  Bit_APIFetchNearLocation.fetchNearbyLocations(cordinate[0],cordinate[1]);
        try {
            if(cordinate[0] == null || cordinate[1] == null){
                throw new MoviesException("निर्देशांक सापडले नाहीत");
            }

            if(bit_nearLocation == null){
                throw new MoviesException("जवळची ठिकाणे सापडली नाहीत!");
            }
        } catch (MoviesException e) {
            ProjectNavBar.showDialog("Error","images/warning.png",e.getMessage());
        }

        if(bit_nearLocation != null){
            bit_nearLocationPane.getChildren().clear();
            for (int i = 0; i < bit_nearLocation.length; i++) {
                Button bit_nearLocationButton = new Button();
                bit_nearLocationButton.setText(bit_nearLocation[i]);
                bit_nearLocationButton.setPrefSize(200, 70);
                bit_nearLocationButton.setStyle("-fx-background-color: #102C57 ; -fx-text-fill: #FFCBCB; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 25px;");
                bit_nearLocationButton.setCursor(Cursor.HAND);
                bit_nearLocationButton.setOnAction(e->{
                    AppState.getInstance().setLocation(bit_nearLocationButton.getText());
                    ProjectNavBar.showDialog("Success","images/success.png","Location set to "+bit_nearLocationButton.getText());
                    landing.redirectToHome();
                });
                bit_nearLocationPane.getChildren().add(bit_nearLocationButton);
            }
        }else{
            System.out.println("Location not found");
        }
    }

    public VBox getlocationScene() {
        return bit_locationPage;
    }
}