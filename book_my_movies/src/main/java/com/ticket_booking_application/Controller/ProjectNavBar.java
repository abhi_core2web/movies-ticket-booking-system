package com.ticket_booking_application.Controller;

import com.google.api.Context;
import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.MoviesException;


import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ProjectNavBar  {
    
    Button bit_navLogin;
    // Label for profile text
    Label bit_navProfileText;
    // Label for user name text
    private HBox bit_navBar;
    // Label for location text
    Label bit_navLocationText;
    // Label for user name text
    Label bit_menuUserNameText;

    

    Landing landing = new Landing();

    public HBox createNavBar() {
        // Main logo of navbar
        Image bit_navLogo = new Image("Images/logo.jpg"); // Image for logo
        ImageView bit_navLogoObj = new ImageView(bit_navLogo); // ImageView for logo
        bit_navLogoObj.setFitHeight(100); // Set height of logo
        bit_navLogoObj.setFitWidth(200); // Set width of logo
        bit_navLogoObj.setPreserveRatio(true); // Preserve the ratio of logo

        // Search bar of navbar
        TextField bit_searchBar = new TextField(); // TextField for search bar
        bit_searchBar.setPromptText("Search Here"); // Set prompt text for search bar
        bit_searchBar.setFocusTraversable(false); // Set focus traversable to false
        bit_searchBar.setPrefWidth(600); // Set width of search bar
        bit_searchBar.setPrefHeight(50); // Set height of search bar
        bit_searchBar.setStyle("-fx-background-color: white; -fx-background-radius: 10; -fx-font-weight: bold; -fx-font-size:20"); // Set                                                                                                  // bar
        bit_searchBar.setCursor(Cursor.TEXT); // Set cursor to text
        bit_searchBar.setPadding(new Insets(10, 10, 10, 60)); // Set padding for search bar
        bit_searchBar.setFont(Font.font("Arial")); // Set font for search bar

        // Search icon of navbar
        Image bit_searchIcon = new Image("Images/search.png"); // Image for search icon
        ImageView bit_searchIconObj = new ImageView(bit_searchIcon); // ImageView for search icon
        bit_searchIconObj.setFitHeight(50); // Set height of search icon
        bit_searchIconObj.setFitWidth(50); // Set width of search icon
        bit_searchIconObj.setPreserveRatio(true); // Preserve the ratio of search icon
        bit_searchIconObj.setCursor(Cursor.HAND); // Set cursor to hand
        bit_searchIconObj.setOnMouseClicked(e -> {
            String searchTerm = bit_searchBar.getText();
            try {
                if(searchTerm.isEmpty()) {
                    throw new MoviesException("कृपया शोध शब्द प्रविष्ट करा");
                } else {
                    landing.redirectToMovieInfo(searchTerm);
                }
            }catch(MoviesException ex) {
                showDialog("Error", "Images/warning.png", ex.getMessage());
            }
        });
        


        
        StackPane.setAlignment(bit_searchIconObj, Pos.CENTER_RIGHT); // Align search icon to center right
        // StackPane for search bar and search icon
        StackPane bit_searchIconStack = new StackPane(bit_searchBar, bit_searchIconObj); // StackPane for search bar and

        // Login and Profile button of navbar
        Rectangle bit_navLocation = new Rectangle(50, 50); // Rectangle for location
        bit_navLocation.setFill(Color.WHITE); // Fill color for location
        bit_navLocation.setStroke(Color.BLACK); // Stroke color for location
        bit_navLocation.setStrokeWidth(2); // Stroke width for location
        bit_navLocation.setArcWidth(30); // Arc width for location
        bit_navLocation.setArcHeight(20); // Arc height for location
        bit_navLocation.setWidth(130); // Width for location

        // Text for location
        bit_navLocationText = new Label("Location"); // Label for location
        bit_navLocationText.setFont(Font.font("Arial", 20)); // Set font for location
        bit_navLocationText.setTextFill(Color.BLACK); // Set text color for location
        bit_navLocationText.setStyle("-fx-font-weight: bold;"); // Set style for location
        bit_navLocationText.setCursor(Cursor.HAND); // Set cursor to hand
        bit_navLocationText.setOnMouseClicked(e -> {
            landing.redirectToLocation(); // Redirect to location page
        });

        // StackPane for location and text
        StackPane bit_navLocationStack = new StackPane(bit_navLocation, bit_navLocationText); // StackPane for location
        // For user information
        Circle bit_navProfile = new Circle(35); // Circle for profile
        bit_navProfile.setFill(Color.WHITE); // Fill color for profile
        bit_navProfile.setStroke(Color.BLACK); // Stroke color for profile
        bit_navProfile.setStrokeWidth(2); // Stroke width for profile
        bit_navProfile.setCursor(Cursor.HAND); // Set cursor to hand
        bit_navProfile.setOnMouseClicked(e -> {
            landing.redirectToUserInfo();
        });
 
        
        bit_navProfileText = new Label(); // Label for profile text
        bit_navProfileText.setFont(Font.font("Arial", 20)); // Set font for profile text
        bit_navProfileText.setTextFill(Color.BLACK); // Set text color for profile text
        bit_navProfileText.setStyle("-fx-font-weight: bold;-fx-text-fill:white"); // Set style for profile text
        bit_navProfileText.setVisible(false); // Set visibility to false
        bit_navProfileText.setOnMouseClicked(e -> {
            
            ContextMenu contextMenu = new ContextMenu();
    
            MenuItem logoutItem = new MenuItem("Logout");
            logoutItem.setOnAction(event -> {
                AppState.getInstance().setLoggedInUsername(null);
                AppState.getInstance().setLoggedInUserFullName(null);
                AppState.getInstance().setLocation(null);
                bit_isLogin(false);
            });
      
            contextMenu.getItems().add(logoutItem);
    
            contextMenu.show(bit_navProfileText, e.getScreenX(), e.getScreenY());
        });

        // Login button
        bit_navLogin = new Button("Login"); // Button for login
        bit_navLogin.setPrefHeight(50); // Set height for login button
        bit_navLogin.setPrefWidth(100); // Set width for login button
        bit_navLogin.setStyle("-fx-background-color: gray; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_navLogin.setCursor(Cursor.HAND); // Set cursor to hand
        bit_navLogin.setBackground(new Background(new BackgroundFill(Color.WHITE, new CornerRadii(20), Insets.EMPTY))); // Se
        bit_navLogin.setOnAction(e -> {
            landing.redirectToLogin(); // Redirect to login page
        });

        StackPane bit_navProfileStack = new StackPane(bit_navLogin, bit_navProfileText); // StackPane for login button
        // Declare position to each segment of navbar
        HBox bit_leftBox = new HBox(bit_navLogoObj); // HBox for logo
        bit_leftBox.setPadding(new Insets(10, 10, 10, 20)); // Padding for logo
        HBox bit_centerBox = new HBox(20, bit_searchIconStack); // HBox for search icon
        bit_centerBox.setAlignment(Pos.CENTER); // Align search icon to center

        HBox bit_rightBox = new HBox(70, bit_navLocationStack, bit_navProfile, bit_navProfileStack); // HBox for
        bit_rightBox.setAlignment(Pos.CENTER_RIGHT); // Align location, profile, and login button to center right

        // GridPane for Navbar to set position of elements inside it
        GridPane bit_gridPos = new GridPane(); // GridPane for Navbar
        bit_gridPos.add(bit_leftBox, 0, 0); // Add logo to grid
        bit_gridPos.add(bit_centerBox, 1, 0); // Add search icon to grid
        bit_gridPos.add(bit_rightBox, 2, 0); // Add location, profile, and login button to grid
        bit_gridPos.setHgap(250); // Set horizontal gap for grid
        bit_gridPos.setAlignment(Pos.CENTER); // Align grid to center

        // Navbar variable that will going to the top of the page
        bit_navBar = new HBox(bit_gridPos); // HBox for Navbar
        bit_navBar.setMaxHeight(50); // Set height for Navbar
        bit_navBar.setStyle("-fx-background-color: #000000; -fx-background-radius: 50px"); // Set style for Navbar
        bit_navBar.setPadding(new Insets(10, 10, 10, 10)); // Set padding for Navbar



        return bit_navBar;

    }
    
    public static void showDialog(String title,String image, String message) {
        // Create a new stage for the dialog
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.initStyle(StageStyle.TRANSPARENT); // Make the stage transparent for custom styling
        dialogStage.setTitle(title);

        ImageView bit_navLogoObj = new ImageView(new Image(image)); // Image for logo
        bit_navLogoObj.setFitHeight(100); // Set height of logo
        bit_navLogoObj.setFitWidth(100); // Set width of logo
        bit_navLogoObj.setPreserveRatio(true); // Preserve the ratio of logo

        // Create the message label
        Label messageLabel = new Label(message);
        messageLabel.setStyle("-fx-font-size: 20px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-family: Arial;");

        // Create the OK button
        Button okButton = new Button("OK");
        okButton.setStyle("-fx-background-color: #3498db; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 16px; -fx-padding: 10 20; -fx-background-radius: 15; -fx-border-radius: 5; -fx-cursor: hand;");
        okButton.setMinWidth(100);
        okButton.setOnAction(e -> {
            FadeTransition fadeOut = new FadeTransition(Duration.seconds(0.5), dialogStage.getScene().getRoot());
            fadeOut.setFromValue(1.0);
            fadeOut.setToValue(0.0);
            fadeOut.setOnFinished(event -> dialogStage.close());
            fadeOut.play();
        });

        // Create a VBox layout for the dialog
        VBox vbox = new VBox(30, bit_navLogoObj,messageLabel, okButton);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(20));
        vbox.setStyle("-fx-background-radius: 30; -fx-border-radius: 10;" +
                      "-fx-background-color: linear-gradient(to bottom right, #ffffff, #2C3E50, #000000);" +
                      "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.75), 25, 0.5, 0.0, 0.0);");
        vbox.setPrefWidth(400);
        vbox.setPrefHeight(300);
        vbox.setLayoutY(100);

        // Create a scene for the dialog with a transparent fill
        Scene dialogScene = new Scene(vbox);
        dialogScene.setFill(javafx.scene.paint.Color.TRANSPARENT);
        dialogStage.setScene(dialogScene);

        // Ensure the dialog is centered on the screen
        dialogStage.centerOnScreen();
        // Existing fade transition code
        FadeTransition fadeIn = new FadeTransition(Duration.seconds(2.0), vbox);
        fadeIn.play(); // Assuming you want to play the fade-in transition

        // Create and configure the scale transition
        ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(0.9), vbox);
        scaleTransition.setFromX(0.1);
        scaleTransition.setFromY(0.1);
        scaleTransition.setToX(1.0);
        scaleTransition.setToY(1.0);

        FadeTransition fadeOut = new FadeTransition(Duration.seconds(0.9), vbox);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setOnFinished(e -> dialogStage.close());


        // Play the scale transition
        scaleTransition.play();

        // Show the dialog and wait for it to be closed before returning
        dialogStage.showAndWait();
    }

    public void bit_isLogin(boolean isLogin) {
        String location = AppState.getInstance().getLocation();
        if (isLogin) {
            bit_navLogin.setVisible(false);
            bit_navProfileText.setVisible(true);
            System.out.println("User is logged in");
            bit_navProfileText.setText(AppState.getInstance().getLoggedInUsername());
            bit_navLocationText.setText(location);
    
        } else {
            bit_navLogin.setVisible(true);
            bit_navProfileText.setVisible(false);
            System.out.println("User is not logged in");
        }
    }
}
