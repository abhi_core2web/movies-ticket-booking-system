package com.ticket_booking_application.Controller;

import java.util.HashMap;
import java.util.Map;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.WriteResult;
import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.FirebaseConfiguration;
import com.ticket_booking_application.services.Movies;
import com.ticket_booking_application.services.MoviesException;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class Login {

    
    Landing landing;
    HBox root;
    private Label bit_loginStatus;
    TextField bit_usernametextField;
    PasswordField bit_passwordField;
    String thisPageName = "Login";

    /*
     * Author Name : Yash Narawade.
     * This is a login Page that simple take the user's login Id and Password
     * Input : Login Id and Password.
     * Output : Login Successfull.
     * 
     */

    public Login(Landing landing) {
        this.landing = landing;
        createLoginPage();
    }

    Home home = new Home(landing);
    Movies movies = new Movies();
    FirebaseConfiguration bit_configFirebase = new FirebaseConfiguration();

    /*
     * This method is used to create the login page. It takes the user's login Id
     * and Password as input and checks if the user is valid or not.
     * 
     * @Auth : Yash Narawa
     */
    public void createLoginPage() {
        
        Rectangle bit_rectangle = new Rectangle(680, 600);
        bit_rectangle.setArcHeight(50);
        bit_rectangle.setArcWidth(50);
        bit_rectangle.setStyle("-fx-fill: rgba(255, 255, 255, 0.5);");

        Label bit_usernameLabel = new Label("👤  Username");
        bit_usernametextField = new TextField();
        Label bit_passwordLabel = new Label("👁️‍🗨️  Password");
        bit_passwordField = new PasswordField();

        bit_configureFields(bit_usernametextField, bit_passwordField);
        bit_configureLabels(bit_usernameLabel, bit_passwordLabel);

        VBox bit_loginFields = new VBox(20, bit_usernameLabel, bit_usernametextField, bit_passwordLabel,
                bit_passwordField);
        StackPane.setMargin(bit_loginFields, new Insets(350, 0, 0, 1100));

        Button bit_loginButton = new Button("Login");
        bit_configureLoginBtn(bit_loginButton);
        bit_loginButton.setCursor(Cursor.HAND);

        // Change button color when pressed and released
        bit_loginButton.setOnMousePressed(e -> bit_loginButton.setStyle(
                "-fx-background-color: #555555; -fx-text-fill: #ffffff; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;"));
        bit_loginButton.setOnMouseReleased(e -> bit_loginButton.setStyle(
                "-fx-background-color: #000000; -fx-text-fill: #ffffff; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;"));

        bit_loginButton.setOnAction(e -> {
            bit_handleLogin(bit_usernametextField.getText(), bit_passwordField.getText());
            bit_usernametextField.clear();
            bit_passwordField.clear();
        });

        HBox bit_loginButtonBox = new HBox(bit_loginButton);
        bit_loginButtonBox.setAlignment(Pos.CENTER);
        StackPane.setMargin(bit_loginButtonBox, new Insets(10, 50, 10, 50));

        Label bit_signUpButton = new Label("Don't have an account? Sign Up");
        bit_signUpButton
                .setStyle("-fx-text-fill: #000000; -fx-font-weight: bold; -fx-font-size: 20; -fx-cursor: hand;");
        bit_signUpButton.setOnMouseClicked(e -> {
            showSignUpDialog();
        });

        Label bit_skipToHome = new Label("Skip...");
        bit_skipToHome.setStyle("-fx-text-fill: #000000; -fx-font-size: 20; -fx-cursor: hand;");
        bit_skipToHome.setOnMouseClicked(e -> {
            landing.redirectToHome();

        });

        bit_loginStatus = new Label();
        bit_loginStatus.setStyle("-fx-text-fill: green; -fx-font-weight: bold; -fx-font-size: 20; -fx-cursor: hand;");

        VBox bit_signUpSkipMsg = new VBox(30, bit_signUpButton, bit_skipToHome, bit_loginStatus);
        bit_signUpSkipMsg.setAlignment(Pos.CENTER);

        StackPane.setMargin(bit_rectangle, new Insets(50, 0, 0, 1040));

        GridPane bit_infoOnLogin = new GridPane();
        bit_infoOnLogin.add(bit_loginFields, 0, 0);
        bit_infoOnLogin.add(bit_loginButtonBox, 0, 1);
        bit_infoOnLogin.add(bit_signUpSkipMsg, 0, 2);
        bit_infoOnLogin.setVgap(40);

        StackPane.setMargin(bit_infoOnLogin, new Insets(250, 0, 0, 1100));
        StackPane bit_loginStack = new StackPane(bit_rectangle, bit_infoOnLogin);

        root = new HBox(bit_loginStack);
        root.setStyle(
                "-fx-background-image: url('images/bgc.jpg'); -fx-background-size: cover; -fx-background-position: center;");
    }

    public void bit_configureFields(TextField bit_usernametextField, PasswordField bit_passwordField) {
        bit_usernametextField.setPromptText("Enter Username");
        bit_usernametextField.setFocusTraversable(true);
        bit_usernametextField.setPrefWidth(550);
        bit_usernametextField.setPrefHeight(50);
        bit_usernametextField.setStyle(
                "-fx-background-color: #ffffff; -fx-text-fill: #000000; -fx-border-color: #000000; -fx-border-width: 1px; -fx-border-radius: 10px; -fx-background-radius: 10px;");

        bit_passwordField.setPromptText("Enter Password");
        bit_passwordField.setFocusTraversable(true);
        bit_passwordField.setPrefWidth(550);
        bit_passwordField.setPrefHeight(50);
        bit_passwordField.setStyle(
                "-fx-background-color: #ffffff; -fx-text-fill: #000000; -fx-border-color: #000000; -fx-border-width: 1px; -fx-border-radius: 10px; -fx-background-radius: 10px;");
        bit_passwordField.requestFocus();

    }

    public void bit_configureLabels(Label bit_usernameLabel, Label bit_passwordLabel) {
        bit_usernameLabel.setTextFill(Color.web("#000000"));
        bit_usernameLabel.setTranslateY(10);
        bit_usernameLabel.setStyle(
                " -fx-padding: 10,10,10,10; -fx-font-size: 25; -fx-font-weight: bold; -fx-text-fill: #000000;");

        bit_passwordLabel.setTextFill(Color.web("#000000"));
        bit_passwordLabel.setTranslateY(10);

        bit_passwordLabel.setStyle(
                " -fx-padding: 10,10,10,10; -fx-font-size: 25; -fx-font-weight: bold; -fx-text-fill: #000000;");
    }

    public void bit_configureLoginBtn(Button bit_loginButton) {
        bit_loginButton.setPrefWidth(400);
        bit_loginButton.setMaxHeight(50);
        bit_loginButton.setAlignment(Pos.CENTER);
        bit_loginButton.setStyle(
                "-fx-background-color: #000000; -fx-text-fill: #ffffff; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px; -fx-border-radius: 10px;");
    }

    public void bit_handleLogin(String username, String password) {
        
        try{
                if (username == null || username.trim().isEmpty()){
        
                        throw new MoviesException("वापरकर्तानाव रिक्त असू शकत नाही!");
                }
                if(password == null || password.trim().isEmpty()) {

                        throw new MoviesException("पासवर्ड रिक्त असू शकत नाही!");
                }
               
        }catch(MoviesException e){
                ProjectNavBar.showDialog("Error","images/warning.png", e.getMessage());
        }       

        try {
            if (bit_configFirebase.authenticateUser(username,username, password)) {
                bit_loginStatus.setText("Login Successful");
                AppState.getInstance().setLoggedIn(true);
                bit_configFirebase.getData(username,"credential");
                bit_usernametextField.clear();
                bit_passwordField.clear();
                bit_loginStatus.setText("");
                ProjectNavBar.showDialog("Success","images/success.png", "लॉगिन यशस्वी झाले आहे!");
                landing.redirectToLocation();
            } else {
                ProjectNavBar.showDialog("Error","images/warning.png", "अवैध वापरकर्तानाव किंवा पासवर्ड !");
                bit_loginStatus.setTextFill(Color.RED);

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

   
        public void showSignUpDialog() {
                Dialog<Void> dialog = new Dialog<>();
                dialog.initStyle(StageStyle.TRANSPARENT); // Make dialog background transparent
                dialog.setTitle("Sign Up");

                // Background rectangle with gradient and border radius

                Rectangle backgroundRectangle = new Rectangle(880, 600);
                backgroundRectangle.setArcHeight(30);
                backgroundRectangle.setArcWidth(30);
                backgroundRectangle.setFill(Color.rgb(240, 240, 240)); // Light gray fill color
                backgroundRectangle.setStroke(Color.BLACK); // Black stroke color
                backgroundRectangle.setStrokeWidth(2); // Stroke width
                backgroundRectangle.setStyle("-fx-background-radius: 10;");
                backgroundRectangle.setOpacity(0.9);
                backgroundRectangle.setLayoutY(50);




                // Title text
                Text titleText = new Text("Registration Form");
                titleText.setFont(Font.font("Arial", 40));
                titleText.setFill(Color.BLACK);

                // Form fields
                Label nameLabel = new Label("Name");
                TextField nameTextField = new TextField();

                Label emailLabel = new Label("Email");
                TextField emailTextField = new TextField();

                Label usernameLabel = new Label("Username");
                TextField usernameTextField = new TextField();

                Label passwordLabel = new Label("Password");
                PasswordField passwordField = new PasswordField();

                // Configure fields and labels
                bit_configureFields(nameTextField, emailTextField, usernameTextField, passwordField);
                bit_configureLabels(nameLabel, emailLabel, usernameLabel, passwordLabel);

                // Sign up button
                Button signUpButton = new Button("Sign up");
                bit_configureSignUpBtn(signUpButton);

                // Button styling on mouse press and release
                signUpButton.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_PRESSED, e -> signUpButton.setStyle(
                        "-fx-background-color: #light blue; -fx-text-fill: #ffffff; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;"));
                signUpButton.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_RELEASED, e -> signUpButton.setStyle(
                        "-fx-background-color: #000000; -fx-text-fill: #ffffff; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;"));

                // Sign up action
                signUpButton.setOnAction(e -> {
                        try {
                                if (nameTextField.getText().isEmpty() || emailTextField.getText().isEmpty() || usernameTextField.getText().isEmpty() || passwordField.getText().isEmpty()) {
                                        throw new MoviesException("सर्व क्षेत्रे आवश्यक आहेत");
                                }
                        } catch (MoviesException ex) {
                                ProjectNavBar.showDialog("Error","images/warning.png", ex.getMessage());
                                return;
                        }
                        handleSignup(usernameTextField.getText(), passwordField.getText(), nameTextField.getText(), emailTextField.getText());
                        dialog.close();
                        landing.redirectToLogin();

                        // Fade out animation
                        FadeTransition fadeOut = new FadeTransition(Duration.seconds(0.5), backgroundRectangle);
                        fadeOut.setFromValue(1.0);
                        fadeOut.setToValue(0.0);
                        fadeOut.setOnFinished(f -> dialog.close());
                        fadeOut.play();
                });

                // Layout setup
                VBox signUpFields = new VBox(20, titleText, nameLabel, nameTextField, emailLabel, emailTextField,
                        usernameLabel, usernameTextField, passwordLabel, passwordField);
                signUpFields.setAlignment(Pos.CENTER);

                HBox signUpButtonBox = new HBox(signUpButton);
                signUpButtonBox.setAlignment(Pos.CENTER);
                signUpButtonBox.setPadding(new Insets(20));

                VBox dialogContent = new VBox(70, signUpFields, signUpButtonBox);
                dialogContent.setAlignment(Pos.CENTER);
                dialogContent.setPadding(new Insets(100, 20, 40, 20));
                dialogContent.setStyle("-fx-background-color: linear-gradient(to bottom right, #ffffff, #2C3E50, #000000);" +
                      "-fx-background-radius: 10;" +
                      "-fx-border-color: #000000;" +
                      "-fx-border-width: 2px;" +
                      "-fx-border-radius: 10;" +
                      "-fx-effect: dropshadow(gaussian, rgba(0,0,0,0.75), 10, 0.5, 0.0, 0.0);");

                dialog.getDialogPane().setContent(new StackPane(backgroundRectangle, dialogContent));
                dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);

                // Scale transition for dialog showing effect
                ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(300), dialogContent);
                scaleTransition.setFromX(0.8);
                scaleTransition.setFromY(0.8);
                scaleTransition.setToX(1.0);
                scaleTransition.setToY(1.0);
                scaleTransition.setCycleCount(1);
                scaleTransition.setAutoReverse(false);

                dialog.showingProperty().addListener((obs, wasShowing, isNowShowing) -> {
                        if (isNowShowing) {
                        scaleTransition.play();
                        }
                });

                dialog.showAndWait();
        }
    public void bit_configureFields(TextField bit_nametextField, TextField bit_emailtextField,
            TextField bit_usrtextField, PasswordField bit_passtextField) {
        bit_nametextField.setPromptText("Enter Name");
        bit_nametextField.setFocusTraversable(true);
        bit_nametextField.setMaxWidth(550);
        bit_nametextField.setMinHeight(50);
        bit_nametextField.setStyle(
                "-fx-background-color: #ffffff; -fx-text-fill: #000000; -fx-border-color: #000000; -fx-border-width: 1px; -fx-border-radius: 10px; -fx-background-radius: 10px;");

        bit_emailtextField.setPromptText("Enter Email");
        bit_emailtextField.setFocusTraversable(true);
        bit_emailtextField.setMaxWidth(550);
        bit_emailtextField.setMinHeight(50);
        bit_emailtextField.setStyle(
                "-fx-background-color: #ffffff; -fx-text-fill: #000000; -fx-border-color: #000000; -fx-border-width: 1px; -fx-border-radius: 10px; -fx-background-radius: 10px;");

        bit_usrtextField.setPromptText("Enter Username");
        bit_usrtextField.setFocusTraversable(true);
        bit_usrtextField.setMaxWidth(550);
        bit_usrtextField.setMinHeight(50);
        bit_usrtextField.setStyle(
                "-fx-background-color: #ffffff; -fx-text-fill: #000000; -fx-border-color: #000000; -fx-border-width: 1px; -fx-border-radius: 10px; -fx-background-radius: 10px;");

        bit_passtextField.setPromptText("Enter Password");
        bit_passtextField.setFocusTraversable(true);
        bit_passtextField.setMaxWidth(550);
        bit_passtextField.setMinHeight(50);
        bit_passtextField.setStyle(
                "-fx-background-color: #ffffff; -fx-text-fill: #000000; -fx-border-color: #000000; -fx-border-width: 1px; -fx-border-radius: 10px; -fx-background-radius: 10px;");
    }

    public void bit_configureLabels(Label bit_nameLabel, Label bit_emailLabel, Label bit_usrLabel,
            Label bit_passLabel) {
        bit_nameLabel.setFont(new Font(25));
        bit_nameLabel.setTextFill(Color.web("#000000"));
        bit_nameLabel.setStyle(
                "-fx-padding: 10,10,10,10; -fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: #000000;");

        bit_emailLabel.setFont(new Font(25));
        bit_emailLabel.setTextFill(Color.web("#000000"));
        bit_emailLabel.setStyle(
                "-fx-padding: 10,10,10,10; -fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: #000000;");

        bit_usrLabel.setFont(new Font(25));
        bit_usrLabel.setTextFill(Color.web("#000000"));
        bit_usrLabel.setStyle(
                "-fx-padding: 10,10,10,10; -fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: #000000;");

        bit_passLabel.setFont(new Font(25));
        bit_passLabel.setTextFill(Color.web("#000000"));
        bit_passLabel.setStyle(
                "-fx-padding: 10,10,10,10; -fx-font-size: 20; -fx-font-weight: bold; -fx-text-fill: #000000;");
    }

    public void bit_configureSignUpBtn(Button bit_signupButton) {
        bit_signupButton.setPrefWidth(400);
        bit_signupButton.setPrefHeight(50);
        bit_signupButton.setStyle(
                "-fx-background-color: #000000; -fx-text-fill: #ffffff; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
    }


    public void handleSignup(String username, String password, String name, String email) {
       
       try{
        
        Map<String, Object> data = new HashMap<>();
        data.put("username", username);
        data.put("password", password);
        data.put("name", name);
        data.put("email", email);

        bit_configFirebase.addData(username, "credential", data); 
        ProjectNavBar.showDialog("Success","images/success.png", "वापरकर्ता यशस्वीरित्या नोंदणीकृत झाला आहे!");   
       }catch(Exception e){
           ProjectNavBar.showDialog("Error","images/warning.png", e.getMessage());
       }
    }

    public HBox getLoginScene() {
        return root;
    }
}
