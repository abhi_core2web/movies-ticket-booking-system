package com.ticket_booking_application.Controller;

import java.util.HashMap;
import java.util.Map;

import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.FirebaseConfiguration;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Payment extends ProjectNavBar {

    BorderPane root;
    Landing landing;
    VBox centerPayment;

    ImageView bit_movieInfoImageView;
    Label bit_title;
    Label bit_timeAndDate;
    Label bit_theatreName;
    Label bit_langMovie;
    Label bit_subTotal;
    Label gstLabel;
    Label totalLabel;
    private int currentPageNumber = 5;

    public Payment(Landing landing) {
        this.landing = landing;
        createPaymentPage();
    }

    private void createPaymentPage() {
        Button bit_backButton = new Button("Back");
        bit_backButton.setStyle(
                "-fx-background-color: #1e1e1e; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_backButton.setCursor(Cursor.HAND);
        bit_backButton.setOnAction(e -> {
            landing.redirectToMovieInfo("War");
        });
        StackPane.setAlignment(bit_backButton, Pos.TOP_LEFT);
        HBox navBar = createNavBar();
        HBox mainPayment = createMainPage();

        StackPane mainPaymentBox = new StackPane(mainPayment, bit_backButton);
        root = new BorderPane();
        root.setStyle("-fx-background-color: gray;");
        root.setTop(navBar);
        root.setCenter(mainPaymentBox);
    }

    private HBox createMainPage() {

        HBox mainPayment = new HBox(40, createLeftSide(), createCenterSide(), createRightSide());
        mainPayment.setAlignment(Pos.CENTER);

        mainPayment.setStyle(
                "-fx-background-color: #f7f9fc; " + // Light gray-blue background
                        "-fx-background-radius: 10; " +
                        "-fx-padding: 20; " +
                        "-fx-border-color: #3498db; " + // Bright blue border
                        "-fx-border-width: 2; " +
                        "-fx-border-radius: 10;");

        // Add drop shadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setColor(Color.rgb(0, 0, 0, 0.25));
        dropShadow.setRadius(10);
        mainPayment.setEffect(dropShadow);
        return mainPayment;
    }

    private VBox createLeftSide() {

        Image bit_creditCardImage = new Image("images/credit.png");
        ImageView bit_creditCardImageView = new ImageView(bit_creditCardImage);
        bit_creditCardImageView.setFitHeight(50);
        bit_creditCardImageView.setFitWidth(50);

        Button bit_creditCardLabel = new Button("Credit Card");
        bit_creditCardLabel.setStyle(
                "-fx-background-color: #1e1e1e; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_creditCardLabel.setCursor(Cursor.HAND);
        bit_creditCardLabel.setOnAction(e -> {
            createCreditCardForm();
        });

        HBox bit_creditCard = new HBox(20, bit_creditCardImageView, bit_creditCardLabel);

        Image bit_debitCardImage = new Image("images/debit.png");
        ImageView bit_debitCardImageView = new ImageView(bit_debitCardImage);
        bit_debitCardImageView.setFitHeight(50);
        bit_debitCardImageView.setFitWidth(50);

        Button bit_debitCardLabel = new Button("Debit Card");
        bit_debitCardLabel.setStyle(
                "-fx-background-color: #1e1e1e; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_debitCardLabel.setCursor(Cursor.HAND);
        bit_debitCardLabel.setOnAction(e -> {
            createDebitCardForm();
        });

        HBox bit_debitCard = new HBox(20, bit_debitCardImageView, bit_debitCardLabel);

        Image bit_upiImage = new Image("images/upi.png");
        ImageView bit_upiImageView = new ImageView(bit_upiImage);
        bit_upiImageView.setFitHeight(50);
        bit_upiImageView.setFitWidth(50);
        Button bit_upiLabel = new Button("UPI");
        bit_upiLabel.setStyle(
                "-fx-background-color: #1e1e1e; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_upiLabel.setCursor(Cursor.HAND);
        bit_upiLabel.setOnAction(e -> {
            createUpiPaymentForm();
        });

        HBox bit_upi = new HBox(20, bit_upiImageView, bit_upiLabel);

        VBox leftSidePayment = new VBox(30, bit_creditCard, bit_debitCard, bit_upi);
        leftSidePayment.setStyle(
                "-fx-background-color: #2c3e50; " + // Dark blue background for a professional look
                        "-fx-background-radius: 40px; " +
                        "-fx-border-color: #ecf0f1; " + // Light border color for contrast
                        "-fx-border-width: 2px; " +
                        "-fx-border-radius: 40px;");
        leftSidePayment.setMaxHeight(750);
        leftSidePayment.setMinWidth(350);
        leftSidePayment.setPadding(new Insets(70));

        return leftSidePayment;
    }

    private VBox createCenterSide() {
        centerPayment = new VBox(20);
        centerPayment.setStyle(
            "-fx-background-color: linear-gradient(to bottom, rgba(52, 152, 219, 0.8), rgba(41, 128, 185, 0.8)); " + // Gradient background with opacity
            "-fx-background-radius: 40px; " +
            "-fx-border-color: #3498db; " +
            "-fx-border-width: 2px; " +
            "-fx-border-radius: 40px;"
        );
        

        centerPayment.setMinWidth(650);
        centerPayment.setMaxHeight(750);
        centerPayment.setAlignment(Pos.CENTER);
        centerPayment.setPadding(new Insets(30));

        Label bit_paymentLabel = new Label("Payment Details");
        bit_paymentLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30");
        centerPayment.getChildren().add(bit_paymentLabel);
        return centerPayment;
    }

    

    private VBox createRightSide() {


        bit_movieInfoImageView = new ImageView();
        bit_movieInfoImageView.setFitHeight(250);
        bit_movieInfoImageView.setPreserveRatio(true);
        bit_movieInfoImageView.setSmooth(true);

        bit_title = new Label("Movie Name: Anime Movie 1");
        bit_title.setStyle("-fx-text-fill: white;  -fx-font-weight: bold; -fx-font-size: 30");
        bit_title.setMaxWidth(430);
        bit_title.setWrapText(true);
        bit_langMovie = new Label("Language: English");
        bit_langMovie.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        bit_timeAndDate = new Label("Time and Date: 12:00 PM, 12th August 2021");
        bit_timeAndDate.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        bit_theatreName = new Label("Theatre Name: PVR Cinemas");
        bit_theatreName.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");

        VBox bit_movieTheInfo = new VBox(30, bit_title, bit_langMovie, bit_timeAndDate, bit_theatreName);


        Label bit_seatInfo = new Label("Seat Number");
        bit_seatInfo.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30");

        Label bit_normalSeat = new Label("Normal Seat: A4");
        bit_normalSeat.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        Label bit_premiumSeat = new Label("premium Seat: B1");
        bit_premiumSeat.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");

        HBox bit_seatInfoBox = new HBox(40, bit_normalSeat, bit_premiumSeat);
        bit_seatInfoBox.setStyle(
            "-fx-background-color: linear-gradient(to bottom, #34495e, #2c3e50); " + // Gradient background from dark blue-gray to darker blue
            "-fx-text-fill: white; " + // White text color
            "-fx-font-weight: bold; " + // Bold font weight
            "-fx-font-size: 20; " + // Font size
            "-fx-border-radius: 30px; " + // Rounded corners
            "-fx-padding: 10px; " + // Padding inside the HBox
            "-fx-border-color: #2c3e50; " + // Border color for contrast
            "-fx-border-width: 2px; " +
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);" // Shadow effect
        );
        bit_seatInfoBox.setMinHeight(50);
        bit_seatInfoBox.setMaxWidth(600);
        bit_seatInfoBox.setAlignment(Pos.CENTER);

        
        HBox bit_movieInfo = new HBox(20, bit_movieInfoImageView, bit_movieTheInfo);
        bit_movieInfo.setStyle(
            "-fx-background-color: linear-gradient(to bottom, #34495e, #2c3e50); " + // Gradient background from dark blue-gray to darker blue
            "-fx-padding: 20px; " + // Padding inside the HBox
            "-fx-background-radius: 20px; " + // Rounded corners
            "-fx-border-color: #2c3e50; " + // Border color for contrast
            "-fx-border-width: 2px; " +
            "-fx-border-radius: 20px; " +
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);" // Shadow effect
        );

        

        Label bit_paymentLabel = new Label("Payment Details");
        bit_paymentLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30");

        bit_subTotal = new Label("Sub Total: $20");
        bit_subTotal.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        gstLabel = new Label("Taxe and GST (18%): $3.6");
        gstLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        totalLabel = new Label("Total Payable: $23.6");
        totalLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        VBox bit_moneyInfo = new VBox(30, bit_subTotal, gstLabel, totalLabel);
        bit_moneyInfo.setStyle(
            "-fx-background-color: linear-gradient(to bottom, #34495e, #2c3e50); " + // Gradient background from dark blue-gray to darker blue
            "-fx-background-radius: 20px; " + // Rounded corners
            "-fx-border-color: #2c3e50; " + // Border color for contrast
            "-fx-border-width: 2px; " + // Border width
            "-fx-border-radius: 20px; " + // Border radius
            "-fx-padding: 20px; " + // Padding inside the VBox
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);" // Shadow effect
        );


        Button bit_payButton = new Button("Pay");
        bit_payButton.setStyle(
                "-fx-background-color: #1e1e1e; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_payButton.setCursor(Cursor.HAND);
        bit_payButton.setOnAction(e -> {
            boolean bit_paymentStatus = handlePayment(AppState.getInstance().getMoviePoster(),AppState.getInstance().getMovieName(), AppState.getInstance().getDate(), AppState.getInstance().getMovieTime(), AppState.getInstance().getTheatreName(), totalLabel.getText());
            
            if(bit_paymentStatus){
                Dialog<ButtonType> dialog = new Dialog<>();
                dialog.setTitle("Payment Verification");
                dialog.setHeaderText("Payment Verification");
                dialog.setContentText("Payment Verified Successfully");
                dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
                // Removed the CLOSE button addition
                dialog.showAndWait();
                landing.redirectToHome();
            }else{
                Dialog<ButtonType> dialog = new Dialog<>();
                dialog.setTitle("Payment Verification");
                dialog.setHeaderText("Payment Verification");
                dialog.setContentText("Payment Failed");
                dialog.showAndWait();
                // To programmatically close the dialog, you would call dialog.close() where appropriate
            }
        });

        VBox RightSidePayment = new VBox(30, bit_movieInfo, bit_seatInfo, bit_seatInfoBox, bit_paymentLabel,
                bit_moneyInfo, bit_payButton);
        RightSidePayment.setStyle(
            "-fx-background-color: #34495e; " + // Dark blue-gray background for a professional look
            "-fx-background-radius: 40px; " +
            "-fx-border-color: #2c3e50; " + // Slightly darker border for contrast
            "-fx-border-width: 2px; " +
            "-fx-border-radius: 40px;"  
        );
                
        RightSidePayment.setMinWidth(700);
        RightSidePayment.setMaxHeight(850);
        RightSidePayment.setPadding(new Insets(20));

        return RightSidePayment;

    }

    private void createDebitCardForm() {
        centerPayment.getChildren().clear();

        Label bit_paymentLabel = new Label("Card Details (Credit Card)");
        bit_paymentLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30");

        Label bit_prompt1 = new Label("Enter your Credit card number");
        bit_prompt1.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardNumber = new TextField();
        bit_cardNumber.setPromptText("Card Number");
        bit_cardNumber.setFocusTraversable(false);
        bit_cardNumber.setMinWidth(500);
        bit_cardNumber.setMinHeight(50);
        bit_cardNumber.setCursor(Cursor.TEXT);
        bit_cardNumber.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardNumber.setPadding(new Insets(10, 10, 10, 20));

        Label bit_prompt2 = new Label("Enter your Credit card expiry date");
        bit_prompt2.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardExpiryDate = new TextField();
        bit_cardExpiryDate.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardExpiryDate.setPromptText("MM/YY");
        bit_cardExpiryDate.setFocusTraversable(false);
        bit_cardExpiryDate.setMinWidth(500);
        bit_cardExpiryDate.setMinHeight(50);
        bit_cardExpiryDate.setCursor(Cursor.TEXT);

        Label bit_prompt3 = new Label("Enter your Credit card CVV");
        bit_prompt3.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardCVV = new TextField();
        bit_cardCVV.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardCVV.setPromptText("CVV");
        bit_cardCVV.setFocusTraversable(false);
        bit_cardCVV.setMinWidth(500);
        bit_cardCVV.setMinHeight(50);
        bit_cardCVV.setCursor(Cursor.TEXT);

        Label bit_prompt4 = new Label("Enter your Credit card holder name");
        bit_prompt4.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardHolderName = new TextField();
        bit_cardHolderName.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardHolderName.setPromptText("Card Holder Name");
        bit_cardHolderName.setFocusTraversable(false);
        bit_cardHolderName.setMinWidth(500);
        bit_cardHolderName.setMinHeight(50);
        bit_cardHolderName.setCursor(Cursor.TEXT);

        Button bit_verifyButton = new Button("Verify");
        bit_verifyButton.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #1e1e1e, #333333); -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_verifyButton.setCursor(Cursor.HAND);
        bit_verifyButton.setOnAction(e -> {
            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.setTitle("Payment Verification");
            dialog.setHeaderText("Payment Verification");
            dialog.setContentText("Payment Verified Successfully");
            dialog.showAndWait();
        });

        VBox cardDetailsFill = new VBox(20, bit_paymentLabel, bit_prompt1, bit_cardNumber, bit_prompt2,
                bit_cardExpiryDate, bit_prompt3, bit_cardCVV, bit_prompt4, bit_cardHolderName, bit_verifyButton);

        cardDetailsFill.setPadding(new Insets(20));
        cardDetailsFill.setStyle(
                "-fx-background-color: linear-gradient(to bottom right, #3498db, #2980b9); -fx-background-radius: 20px; -fx-border-color: #3498db; -fx-border-width: 2px; -fx-border-radius: 20px;");

        centerPayment.getChildren().add(cardDetailsFill);
    }

    private void createCreditCardForm() {
        centerPayment.getChildren().clear();

        Label bit_paymentLabel = new Label("Card Details (Credit Card)");
        bit_paymentLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30");

        Label bit_prompt1 = new Label("Enter your Credit card number");
        bit_prompt1.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardNumber = new TextField();
        bit_cardNumber.setPromptText("Card Number");
        bit_cardNumber.setFocusTraversable(false);
        bit_cardNumber.setMinWidth(500);
        bit_cardNumber.setMinHeight(50);
        bit_cardNumber.setCursor(Cursor.TEXT);
        bit_cardNumber.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardNumber.setPadding(new Insets(10, 10, 10, 20));

        Label bit_prompt2 = new Label("Enter your Credit card expiry date");
        bit_prompt2.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardExpiryDate = new TextField();
        bit_cardExpiryDate.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardExpiryDate.setPromptText("MM/YY");
        bit_cardExpiryDate.setFocusTraversable(false);
        bit_cardExpiryDate.setMinWidth(500);
        bit_cardExpiryDate.setMinHeight(50);
        bit_cardExpiryDate.setCursor(Cursor.TEXT);

        Label bit_prompt3 = new Label("Enter your Credit card CVV");
        bit_prompt3.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardCVV = new TextField();
        bit_cardCVV.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardCVV.setPromptText("CVV");
        bit_cardCVV.setFocusTraversable(false);
        bit_cardCVV.setMinWidth(500);
        bit_cardCVV.setMinHeight(50);
        bit_cardCVV.setCursor(Cursor.TEXT);

        Label bit_prompt4 = new Label("Enter your Credit card holder name");
        bit_prompt4.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        TextField bit_cardHolderName = new TextField();
        bit_cardHolderName.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_cardHolderName.setPromptText("Card Holder Name");
        bit_cardHolderName.setFocusTraversable(false);
        bit_cardHolderName.setMinWidth(500);
        bit_cardHolderName.setMinHeight(50);
        bit_cardHolderName.setCursor(Cursor.TEXT);

        Button bit_verifyButton = new Button("Verify");
        bit_verifyButton.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #1e1e1e, #333333); -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_verifyButton.setCursor(Cursor.HAND);
        bit_verifyButton.setOnAction(e -> {
            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.setTitle("Payment Verification");
            dialog.setHeaderText("Payment Verification");
            dialog.setContentText("Payment Verified Successfully");
            dialog.showAndWait();
        });

        VBox cardDetailsFill = new VBox(20, bit_paymentLabel, bit_prompt1, bit_cardNumber, bit_prompt2,
                bit_cardExpiryDate, bit_prompt3, bit_cardCVV, bit_prompt4, bit_cardHolderName, bit_verifyButton);

        cardDetailsFill.setPadding(new Insets(20));
        cardDetailsFill.setStyle(
                "-fx-background-color: linear-gradient(to bottom right, #3498db, #2980b9); -fx-background-radius: 20px; -fx-border-color: #3498db; -fx-border-width: 2px; -fx-border-radius: 20px;");

        centerPayment.getChildren().add(cardDetailsFill);
    }

    private void createUpiPaymentForm() {
        centerPayment.getChildren().clear();

        ImageView bit_optionImage1 = new ImageView(new Image("images/Google_Pay.png"));
        bit_optionImage1.setFitHeight(60);
        bit_optionImage1.setFitWidth(70);
        ImageView bit_optionImage2 = new ImageView(new Image("images/PhonePe.png"));
        bit_optionImage2.setFitHeight(60);
        bit_optionImage2.setFitWidth(70);
        ImageView bit_optionImage3 = new ImageView(new Image("images/Paytm.png"));
        bit_optionImage3.setFitHeight(60);
        bit_optionImage3.setFitWidth(70);
        ImageView bit_optionImage4 = new ImageView(new Image("images/Amazon.png"));
        bit_optionImage4.setFitHeight(60);
        bit_optionImage4.setFitWidth(70);
        ImageView bit_optionImage5 = new ImageView(new Image("images/bhim.png"));
        bit_optionImage5.setFitHeight(60);
        bit_optionImage5.setFitWidth(70);

        HBox bit_optionImages = new HBox(20, bit_optionImage1, bit_optionImage2, bit_optionImage3, bit_optionImage4,
                bit_optionImage5);

        Label bit_paymentLabel = new Label("UPI Details");
        bit_paymentLabel.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 30");

        TextField bit_upiId = new TextField();
        bit_upiId.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #3498db, #2980b9); -fx-border-color: black; -fx-border-width: 2px; -fx-border-radius: 30px; -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20;");
        bit_upiId.setPromptText("Enter your UPI ID");
        bit_upiId.setFocusTraversable(false);
        bit_upiId.setMinWidth(500);
        bit_upiId.setMinHeight(50);
        bit_upiId.setCursor(Cursor.TEXT);

        Label bit_ybl = new Label("@ybl");
        bit_ybl.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20");
        StackPane.setAlignment(bit_ybl, Pos.CENTER_RIGHT);
        StackPane bit_yblRecongize = new StackPane(bit_upiId, bit_ybl);
        StackPane.setMargin(bit_ybl, new Insets(0, 20, 0, 0));

        Button bit_verifyButton = new Button("Verify");
        bit_verifyButton.setStyle(
                "-fx-background-color: linear-gradient(to bottom, #1e1e1e, #333333); -fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 20; -fx-border-radius: 15px;");
        bit_verifyButton.setCursor(Cursor.HAND);
        bit_verifyButton.setOnAction(e -> {
            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.setTitle("Payment Verification");
            dialog.setHeaderText("Payment Verification");
            dialog.setContentText("Payment Verified Successfully");
            dialog.showAndWait();
        });

        VBox upiDetailsFill = new VBox(20, bit_optionImages, bit_paymentLabel, bit_yblRecongize, bit_verifyButton);

        upiDetailsFill.setPadding(new Insets(20));
        upiDetailsFill.setStyle(
                "-fx-background-color: linear-gradient(to bottom right, #3498db, #2980b9); -fx-background-radius: 20px; -fx-border-color: #3498db; -fx-border-width: 2px; -fx-border-radius: 20px;");

        centerPayment.getChildren().add(upiDetailsFill);
    }

    public void update(){

        bit_movieInfoImageView.setImage(new Image(AppState.getInstance().getMoviePoster()));
        bit_title.setText("Movie Name: " + AppState.getInstance().getMovieName());
        bit_theatreName.setText("Theatre Name: " + AppState.getInstance().getTheatreName());
        bit_timeAndDate.setText("Time and Date: " + AppState.getInstance().getMovieTime() + ", " + AppState.getInstance().getDate());
        bit_langMovie.setText("Language: " + AppState.getInstance().getMovieLanguage());
        bit_subTotal.setText("Sub Total: $" + AppState.getInstance().getSubTotal());
        calculateGST(AppState.getInstance().getSubTotal());
    }

    private void calculateGST(String price) {
        
        double ticketPrice = Double.parseDouble(price.split(": ")[1]);
        double gst = ticketPrice * 0.18;
        double total = ticketPrice + gst;
        gstLabel.setText("Taxe and GST (18%): " + gst+" RS");
        totalLabel.setText("Total Payable: " + total+ " RS");
    }

    public boolean handlePayment(String imgUrl,String movieName, String date, String time, String theatreName, String totalAmout) {
        try {
            FirebaseConfiguration bit_configFirebase = new FirebaseConfiguration();
            Map<String, Object> data = new HashMap<>();
            data.put("id",1);
            data.put("movieName", movieName);
            data.put("MovieDate", date);
            data.put("MovieTime", time);
            data.put("TheatreName", theatreName);
            data.put("TotalAmount", totalAmout);
            data.put("imgUrl", imgUrl);
            data.put("PayedAmount", totalAmout);


            bit_configFirebase.addData(AppState.getInstance().getLoggedInUsername(), movieName, data);

            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    public BorderPane getPaymentScene() {
        return root;
    }

}