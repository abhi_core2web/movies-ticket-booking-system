package com.ticket_booking_application.Controller;

import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.FirebaseConfiguration;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class UserInfo extends ProjectNavBar{
    
    Landing landing;
    VBox bit_userInfoMainPage;
    HBox bit_ticketInfo;
    public UserInfo(Landing landing) {
        this.landing = landing;
        createInfomainPage();
    }
    
    private void createInfomainPage() {
        
        Label label1 = new Label("First Appearence");
        Button b1 = new Button("Click  me");
        b1.setOnAction(r->{
            createInfoHBox();
        });
        bit_userInfoMainPage = new VBox(label1,b1);
        bit_userInfoMainPage.setAlignment(Pos.CENTER);
        bit_userInfoMainPage.setPadding(new Insets(80));
    }

    public void retriveDataFromFirestore() {
        FirebaseConfiguration firebaseConfiguration = new FirebaseConfiguration();
        try{
            firebaseConfiguration.getData(AppState.getInstance().getLoggedInUsername(), AppState.getInstance().getMovieName());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void createInfoHBox() {

        
        ImageView bit_movieImageView = new ImageView(new Image(AppState.getInstance().getMoviePoster()));

        Label bit_movieName = new Label("Title "+ AppState.getInstance().getMovieName());
       
        VBox bit_leftImageBox = new VBox(20,bit_movieImageView,bit_movieName);
       
        Label bit_Date = new Label("Date" + AppState.getInstance().getDate());
        Label bit_theatreName = new Label("Theatre Name: " + AppState.getInstance().getTheatreName());
        // Label bit_payedAmount = new Label("Payed Amount " + AppState.getInstance().getpayedAmount());
        Label bit_timeLabel = new Label("ShowTime: "+AppState.getInstance().getMovieRunTime());
        // VBox bit_rightSideBox = new VBox(bit_timeLabel,bit_Date,bit_theatreName,bit_payedAmount);
        // HBox bit_infoHbox = new HBox(bit_leftImageBox,bit_rightSideBox);
        // bit_userInfoMainPage.getChildren().addAll(bit_infoHbox);

    }

    public VBox getUserInfoScene() {
        return bit_userInfoMainPage;
    }
}