package com.ticket_booking_application.Controller;

import java.text.DecimalFormat;
import java.util.List;

import com.ticket_booking_application.DashBoard.AppState;
import com.ticket_booking_application.DashBoard.Landing;
import com.ticket_booking_application.services.Movies;
import com.ticket_booking_application.services.Bit_APIFetchMovies;
import com.ticket_booking_application.services.Bit_AutoSlidingImageWindow;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

public class Home extends ProjectNavBar {

    /*
     * This Entire code is related to create a Home page of the application with its styling.
     * It Contains the Navbar, Menu Side, and Main Page of the application.
     * Navbar contains the logo, search bar, search icon, location, and profile button.
     * Menu Side contains the welcome message, user name, categories, and about button.
     * Main Page contains the sliding images and main movies photo.
     * @ Auth - Abhishek Bhosale
     */

    
    // Landing class object for redirection
    private Landing landing;
    // BorderPane for root
    private HBox root;
   
    // VBox for Menu Side
    private VBox bit_menuSide;
    // BorderPane for Main Page
    private VBox bit_mainPage;
    // Button for login
   
    // TilePane for movies images
    TilePane bit_boxImages;
    // Label for location tex\

    Label bit_menuUserNameText;

    private int currentPageNumber = 2;


    HBox bit_navBar;

    Movies movies = new Movies();

    // No-argument constructor

    public Home(Landing landing) {
        this.landing = landing;
        createHomePage();
    }
    Bit_APIFetchMovies api = new Bit_APIFetchMovies(); // Create object of APIService


    /*
     * This is important code that is responsible for creating the Home page of the application and return the BorderPane object
     * 
     * @ Auth - Abhishek Bhosale
     */
    public HBox createHomePage() {
        BorderPane rootBorderPane = new BorderPane(); // BorderPane for root
        // root.setStyle("-fx-background-color: #326ED3;"); // Set style for root

        bit_navBar = createNavBar(); // Create navbar   
        bit_mainPage = createMainPage(); // Create main page
        bit_menuSide = createMenuSide(); // Create menu side
        bit_mainPage.setStyle(
            "-fx-background-color: #0E0D0D;" +  // Assuming you want to keep the background color as well
            "-fx-border-color: #37B7C3;" +     // Set the border color
            "-fx-border-width: 2;" +           // Set the border width
            "-fx-border-radius: 10;" +         // Set the border radius
            "-fx-background-radius: 200;"       // Set the background radius
        );

        rootBorderPane.setTop(bit_navBar); // Set navbar to top
        rootBorderPane.setLeft(bit_menuSide); // Set menu side to left
        rootBorderPane.setCenter(bit_mainPage); // Set main page to cen
        BorderPane.setMargin(bit_navBar, new Insets(20) );
        BorderPane.setMargin(bit_mainPage, new Insets(20));
        BorderPane.setMargin(bit_menuSide, new Insets(20));
        root = new HBox(rootBorderPane);
        root.setStyle("-fx-background-color: #088395;");

        return root;
    }

    /*
     * This method is used to create the Navbar with its styling and return the HBox object
     * 
     * @ Auth - Abhishek Bhosale
     */
   

    /*
     * This method is used to create the Main Page with its styling and return the BorderPane object
     * 
     * @ Auth - Abhishek Bhosale
     */
    private VBox createMainPage() {
        // Sliding Images
        api.bit_hitAPIForUpcomingMovies("en"); // Hit API for upcoming movies
        ScrollPane bit_slidingImages = Bit_AutoSlidingImageWindow.bit_createAutoSlidingImageWindow(); // ScrollPane for sliding images
        bit_slidingImages.setStyle("-fx-background-color: #37B7C3;"); // Set style for sliding images
        bit_slidingImages.setPrefHeight(300); // Set preferred height for sliding images
        bit_slidingImages.setFitToWidth(true); // Ensure sliding images fit to width
    
    
        // Main movies photo
        bit_boxImages = new TilePane();
        bit_boxImages.setPrefColumns(5); // Set 5 columns for movie images
        bit_boxImages.setHgap(50); // Set horizontal gap for movie images
        bit_boxImages.setVgap(30); // Set vertical gap for movie images
        bit_boxImages.setPadding(new Insets(10, 0, 0, 0)); // Padding for movie images
        bit_boxImages.setAlignment(Pos.CENTER); // Align movie images to center
        bit_boxImages.setStyle("-fx-background-color: #37B7C3;"); // Set style for movie images
        
        BorderPane bit_boxImagesBorderPane = new BorderPane(); // BorderPane for movie images
        bit_boxImagesBorderPane.setTop(bit_slidingImages);
        bit_boxImagesBorderPane.setCenter(bit_boxImages);
        BorderPane.setMargin(bit_boxImages, new Insets(20, 0, 0, 0));
        VBox bit_contentVbox = new VBox(bit_boxImagesBorderPane); // Combine sliding images and movie images
        bit_contentVbox.setStyle("-fx-background-color: #37B7C3;"); // Set background color for the VBox
        bit_contentVbox.setPadding(new Insets(20)); // Padding for content VBox
    
        ScrollPane bit_mainScrollPane = new ScrollPane(bit_contentVbox); // ScrollPane for combined content
        bit_mainScrollPane.setFitToWidth(true); // Ensure ScrollPane resizes to fit width
        bit_mainScrollPane.setFitToHeight(true); // Ensure ScrollPane resizes to fit height
        bit_mainScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide horizontal scrollbar
        bit_mainScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Hide vertical scrollbar
        bit_mainScrollPane.setStyle("-fx-background-color: #37B7C3;"); // Set style for ScrollPane
        bit_mainScrollPane.setPrefHeight(800); // Set preferred height for ScrollPane
        bit_mainScrollPane.setPrefWidth(1800); // Set preferred width for ScrollPane

    
        VBox bit_mainPageVbox = new VBox(bit_mainScrollPane); // VBox for main page

        return bit_mainPageVbox;
    }
    

    /*
     * This method is used to create the Menu Side with its styling and return the VBox object
     * 
     * @ Auth - Abhishek Bhosale
     */
    private VBox createMenuSide() {
        // Welcome message
        Label bit_welcomeMsg = new Label("👤 Welcome"); // Label for welcome message
        bit_welcomeMsg.setStyle("-fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 40;"); // Set style for
        bit_menuUserNameText = new Label(); // Label for user name
        bit_menuUserNameText.setStyle("-fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 30;");
        VBox bit_menuUserName = new VBox(bit_welcomeMsg, bit_menuUserNameText);

        // Menu side
        Button bit_Home = new Button("Home"); // Button for home
        bit_configureButton(bit_Home); // Configure button
        bit_Home.setOnAction(e -> {
            bit_setMovieImages("horror", "Hindi", 30);
        });
        Button bit_BollyWood = new Button("Bollywood"); // Button for bollywood
        bit_configureButton(bit_BollyWood); // Configure button
        bit_BollyWood.setOnAction(e -> {
            bit_setMovieImages("India", "Hindi", 30);
        });
        Button bit_HollyWood = new Button("Marvel"); // Button for hollywood
        bit_configureButton(bit_HollyWood);
        bit_HollyWood.setOnAction(e -> {
            bit_setMovieImages("marvel", "Hindi", 30);
        });
        Button bit_TollyWood = new Button("Adventure"); // Button for tollywood
        bit_configureButton(bit_TollyWood);
        bit_TollyWood.setOnAction(e -> {
            bit_setMovieImages("adventure", "Hindi", 30);
        });
        Button bit_Marathi = new Button("Marathi");
        bit_configureButton(bit_Marathi);
        bit_Marathi.setOnAction(e -> {
            bit_setMovieImages("Marathi", "Hindi", 30);
        });

        VBox bit_catogriesBox = new VBox(bit_Home, bit_BollyWood, bit_HollyWood, bit_TollyWood, bit_Marathi);
        bit_catogriesBox.setPadding(new Insets(10, 10, 10, 10)); // Padding for categories
        bit_catogriesBox.setSpacing(50); // Spacing for categories
        bit_catogriesBox.setAlignment(Pos.TOP_CENTER); // Align categories to top center

        Button bit_menuAbout = new Button("About"); // Button for about
        bit_configureButton(bit_menuAbout); // Configure button

        ContextMenu bit_menu = new ContextMenu(); // ContextMenu for menu
        MenuItem item1 = new MenuItem("About Us"); // MenuItem for about us
        item1.setOnAction(e -> {
            landing.redirectToAboutUs();
        });
        MenuItem item2 = new MenuItem("Log Out"); // MenuItem for log out
        item2.setOnAction(e -> {
            bit_navLogin.setVisible(true);
            bit_navProfileText.setVisible(false);
            landing.redirectToLogin();
        });

        bit_menu.getItems().addAll(item1, item2); // Add items to menu
        bit_menu.setStyle("-fx-font-size:20");
        bit_menuAbout.setOnAction(event -> {
            bit_menu.show(bit_menuAbout, Side.BOTTOM, 0, 0); // Adjust the position as needed
        });

        // MenuBar
        VBox bit_menuSide = new VBox(50, bit_menuUserName, bit_catogriesBox, bit_menuAbout);
        bit_menuSide.setPadding(new Insets(10, 10, 10, 10));
        bit_menuSide.setStyle(
            "-fx-background-color: #37B7C3;" +
            "-fx-border-radius: 10;" +
            "-fx-background-radius: 10;"
        );

        bit_menuSide.setAlignment(Pos.TOP_CENTER);
        bit_menuSide.setPrefWidth(300);

        return bit_menuSide;
    }

    /*
     * This method is used to configure the button with its styling
     * 
     * @ Auth -Abhishek Bhosale
     */
    private void bit_configureButton(Button button) {
        button.setPrefHeight(50); // Set height of button
        button.setPrefWidth(300); // Set width of button
        button.setCursor(Cursor.HAND); // Set cursor to hand
        button.setStyle("-fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 25; -fx-background-color: transparent; -fx-border-width: 0;");
        button.setOnMouseEntered(e -> {
            button.setStyle("-fx-text-fill: white; -fx-font-weight: bold; -fx-font-size: 25; -fx-background-color: #DF5555; -fx-border-width: 0;");
        });
        button.setOnMouseExited(e -> {
            button.setStyle("-fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 25; -fx-background-color: transparent; -fx-border-width: 0;");
        });
    }

    /*
     * This method creates the image on button on it for home page with its styling
     * and both image and button add into
     * StackPane and it returns the StackPane object
     * Auth-Abhishek Bhosale
     */

     public void bit_setMovieImages(String category, String language, int numberOfMovies) {


        api.fetchMovies(category, language,numberOfMovies); // Fetching first 3 pages (each page contains 10 results
        List<Movies> bit_moviesList = api.getMoviesList(); // Replace with your actual API call

        bit_boxImages.getChildren().clear(); // Clear any existing movie images

        for (Movies movie : bit_moviesList) {
            VBox bit_Vbox = bit_createImageWithButton(movie.getMovieposterUrl(), movie.getMovieName(), movie.getMovieRating());
            bit_boxImages.getChildren().add(bit_Vbox);
        }
    }
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.0");
    
    private VBox bit_createImageWithButton(String imagePath, String movieName, Double movieRating) {
        Label bit_movieRating = new Label("⭐" + DECIMAL_FORMAT.format(movieRating));
        bit_movieRating.setStyle(
            "-fx-text-fill: white; " +
            "-fx-font-weight: bold; " +
            "-fx-font-size: 15; " +
            "-fx-background-color: black; " +
            "-fx-background-radius: 10; " +
            "-fx-padding: 5; " +
            "-fx-border-color: #37B7C3; " +
            "-fx-border-width: 1; " +
            "-fx-border-radius: 10;"
        );
        bit_movieRating.setOpacity(0);
    
        Button bit_bookButton = new Button("Book Now");
        bit_bookButton.setStyle(
            "-fx-background-color: green; " +
            "-fx-text-fill: white; " +
            "-fx-font-weight: bold; " +
            "-fx-font-size: 15; " +
            "-fx-background-radius: 10; " +
            "-fx-border-color: #37B7C3; " +
            "-fx-border-width: 1; " +
            "-fx-border-radius: 10;"
        );
        bit_bookButton.setCursor(Cursor.HAND);
        bit_bookButton.setOpacity(0);
    
        bit_bookButton.setOnMouseEntered(e -> {
            bit_bookButton.setOpacity(1);
            bit_movieRating.setOpacity(1);
            bit_bookButton.setStyle(
                "-fx-background-color: red; " +
                "-fx-text-fill: white; " +
                "-fx-font-weight: bold; " +
                "-fx-font-size: 15; " +
                "-fx-background-radius: 10; " +
                "-fx-border-color: #37B7C3; " +
                "-fx-border-width: 1; " +
                "-fx-border-radius: 10;"
            );
        });
    
        bit_bookButton.setOnMouseExited(e -> {
            bit_bookButton.setOpacity(0);
            bit_bookButton.setStyle("-fx-background-color: green; " +"-fx-text-fill: white; " +"-fx-font-weight: bold; " 
            +"-fx-font-size: 15; " +"-fx-background-radius: 10; " +"-fx-border-color: #37B7C3; " +"-fx-border-width: 1; "
             +"-fx-border-radius: 10;");
        });
    
        bit_bookButton.setOnAction(e -> {
            String bit_movieName = movieName;
            landing.redirectToMovieInfo(bit_movieName);
        });
    
        ImageView bit_movieImgobj = new ImageView(getImageOrDefault(imagePath));
        bit_movieImgobj.setFitWidth(200);
        bit_movieImgobj.setFitHeight(300);
        bit_movieImgobj.setPreserveRatio(true);
        bit_movieImgobj.setCursor(Cursor.HAND);
        bit_movieImgobj.setStyle(
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 0);"
        );
    
        bit_movieImgobj.setOnMouseEntered(e -> {
            bit_bookButton.setOpacity(1);
            bit_movieRating.setOpacity(1);
        });
    
        bit_movieImgobj.setOnMouseExited(e -> {
            bit_bookButton.setOpacity(0);
            bit_movieRating.setOpacity(0);
        });
    
        StackPane.setAlignment(bit_movieRating, Pos.TOP_RIGHT);
        StackPane.setMargin(bit_movieRating, new Insets(10, 10, 0, 0));
    
        StackPane.setAlignment(bit_bookButton, Pos.BOTTOM_CENTER);
        StackPane.setMargin(bit_bookButton, new Insets(10));
    
        StackPane bit_imagesStack = new StackPane(bit_movieImgobj, bit_bookButton, bit_movieRating);
        bit_imagesStack.setStyle(
            "-fx-background-color: #f4f4f4; " +
            "-fx-background-radius: 10; " +
            "-fx-border-color: #37B7C3; " +
            "-fx-border-width: 1; " +
            "-fx-border-radius: 10;"
        );
    
        Label bit_movieNameLabel = new Label(movieName);
        bit_movieNameLabel.setMaxWidth(200);
        bit_movieNameLabel.setWrapText(true);
        bit_movieNameLabel.setStyle(
            "-fx-text-fill: black; " +
            "-fx-font-weight: bold; " +
            "-fx-font-size: 20; " +
            "-fx-padding: 5;"
        );
    
        
    
        VBox bit_image = new VBox(10, bit_imagesStack, bit_movieNameLabel);
        bit_image.setAlignment(Pos.CENTER);
        bit_image.setStyle(
            "-fx-background-color: #EBF4F6; " +
            "-fx-background-radius: 10; " +
            "-fx-border-color: #37B7C3; " +
            "-fx-border-width: 2; " +
            "-fx-border-radius: 10; " +
            "-fx-padding: 10; " +
            "-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.5), 10, 0, 0, 0);"
        );
    
        return bit_image;
    }
    
    private Image getImageOrDefault(String url) {
        try {
            return new Image(url, true);
        } catch (Exception e) {
            return new Image("https://via.placeholder.com/100x150.png?text=No+Image");
        }
    }

    public void bit_isLogin(boolean isLogin) {
        if (isLogin) {
            bit_navLogin.setVisible(false);
            bit_navProfileText.setVisible(true);
            System.out.println("User is logged in");
            bit_navProfileText.setText(AppState.getInstance().getLoggedInUsername());
            bit_menuUserNameText.setText(AppState.getInstance().getLoggedInUserFullName());
            bit_navLocationText.setText(AppState.getInstance().getLocation());
    
        } else {
            bit_navLogin.setVisible(true);
            bit_navProfileText.setVisible(false);
            System.out.println("User is not logged in");
        }
    }

    public HBox getHomePageScene() {
        return root;
    }
    
}
